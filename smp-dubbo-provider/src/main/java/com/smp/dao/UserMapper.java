package com.smp.dao;

import com.smp.entity.User;

/**
 * @author:359428217@qq.com
 * @date:2020/3/29 , 21:30
 * @version:1.0
 * @description:
 **/
public interface UserMapper {

    void insert(User user);
}

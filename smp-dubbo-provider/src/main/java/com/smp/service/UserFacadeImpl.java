package com.smp.service;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.smp.api.UserFacade;
import com.smp.dao.UserMapper;
import com.smp.dto.UserDTO;
import com.smp.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author:359428217@qq.com
 * @date:2020/3/29 , 21:36
 * @version:1.0
 * @description:
 **/
@Slf4j
@Service(version = "1.0.0", interfaceClass = UserFacade.class)
public class UserFacadeImpl implements UserFacade {

    @Autowired
    private UserMapper userMapper;

    @NacosValue(value = "${nacosKey:nacos}", autoRefreshed = true)
    private String nacosKey;

    @Value("${nacos-value}")
    private String nacosValue;

    @Override
    public UserDTO inc(UserDTO userDTO) {
        User user = User.DTOc2Entity(userDTO);
        userMapper.insert(user);
        userDTO.setId(user.getId());
        log.info("insert user: {}", userDTO.toString());
        return userDTO;
    }

    @Override
    public String getNocasValue() {
        return nacosKey + "：" + nacosValue;
    }

    @Override
    public void insertA() {
        // ((UserFacadeImpl) AopContext.currentProxy()).insertB();
        insertB();
    }

    @Transactional
    @Override
    public void insertB() {
        User user = new User();
        user.setName("王五");
        user.setBalance(200L);
        user.setUpTime(new Date());
        userMapper.insert(user);
        int i = 10 / 0;
    }
}

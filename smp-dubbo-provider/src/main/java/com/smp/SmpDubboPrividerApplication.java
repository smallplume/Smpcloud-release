package com.smp;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@MapperScan(value = "com.smp.dao")
@EnableAspectJAutoProxy(exposeProxy = true)
@NacosPropertySource(dataId = "smp-dubbo", autoRefreshed = true)/* 指定配置文件，配置自动刷新 */
public class SmpDubboPrividerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmpDubboPrividerApplication.class, args);
    }

}

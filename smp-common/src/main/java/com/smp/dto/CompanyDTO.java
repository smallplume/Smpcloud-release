package com.smp.dto;

import lombok.Data;

/**
 * @author SmallPlume
 * @title: CompanyDTO
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 23:51
 */
@Data
public class CompanyDTO {

    private Integer id;

    private String name;

    private Integer legalPerson;
}

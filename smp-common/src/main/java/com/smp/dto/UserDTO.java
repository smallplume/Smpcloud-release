package com.smp.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author SmallPlume
 * @title: UserDTO
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 16:38
 */
@Data
public class UserDTO implements Serializable {
    /**
     * ID
     */
    private Integer id;
    /**
     * 用户名
     */
    private String name;
    /**
     * 余额
     */
    private Long balance;
    /**
     * 更改时间
     */
    private Date upTime;
    /**
     * 公司名称
     */
    private String companyName;
}

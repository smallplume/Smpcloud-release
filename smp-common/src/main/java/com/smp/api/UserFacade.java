package com.smp.api;

import com.smp.dto.UserDTO;

/**
 * @author:359428217@qq.com
 * @date:2020/3/29 , 21:33
 * @version:1.0
 * @description:
 **/
public interface UserFacade {

    UserDTO inc(UserDTO userDTO);

    String getNocasValue();

    void insertA();

    void insertB();
}

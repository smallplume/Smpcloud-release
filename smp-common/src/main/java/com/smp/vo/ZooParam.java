package com.smp.vo;

import lombok.Data;

/**
 * @author SmallPlume
 * @title: ZooParam
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/13 14:15
 */
@Data
public class ZooParam {

    private String annimalTypeEnum;
}

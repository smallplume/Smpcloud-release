package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.dto.UserDTO;
import com.smp.server.UserFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author SmallPlume
 * @title: UserController
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 16:55
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserFacade userFacade;

    @Value("${nacosTest:10086}")
    private String nacosTest;

    @PostMapping("/insert")
    public UserDTO insert(@RequestBody UserDTO userDTO) {
        return userFacade.inc(userDTO);
    }

    @GetMapping("/getNacosTest")
    public String getNocasTest() {
        System.out.println("smp-nacos-server-02>>>>>>>>>");
        return "smp-nacos-server-02 >>>>>>>>> " + nacosTest;
    }

    @GetMapping("/query")
    public BaseVo<UserDTO> findUserById(@RequestParam(value = "id") String id) {
        UserDTO user = new UserDTO();
        user.setId(Integer.valueOf(id));
        user.setBalance(123L);
        user.setName("远程调用-user");
        user.setUpTime(new Date());
        log.info("user >>>>>>>>>>>>>>>>>>>> {}",user.toString());
        return new BaseVo(user);
    }

}

package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.entity.DelayJob;
import com.smp.server.DelayJobService;
import com.smp.server.ExecuteJobImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author:359428217@qq.com
 * @date:2020/4/3 , 0:00
 * @version:1.0
 * @description:
 **/
@Slf4j
@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    private DelayJobService delayJobService;

    @GetMapping("/test")
    public String test() {
        System.out.println("进入test方法 " + LocalDateTime.now());
        int value = (int) (Math.random() * 100);
        String v2 = "v" + value;
        redisTemplate.opsForValue().set("name", v2);
        String name = redisTemplate.opsForValue().get("name");
        return name;
    }

    @GetMapping("/delay")
    public BaseVo buildDelayJob(@RequestParam(value = "time") String time) {
        DelayJob delayJob = new DelayJob();
        ExecuteJobImpl executerJob = new ExecuteJobImpl();
        Map<String, String> params = new HashMap<>(1);
        params.put("消息：", "哈哈哈哈哈哈哈哈哈哈哈");
        delayJob.setJobParams(params);
        delayJob.setAClass(executerJob.getClass());
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
        log.info("{} 消息的开始时间 >>>> {}", time, sdf.format(date));
        delayJobService.submitJob(delayJob, Long.valueOf(time), TimeUnit.SECONDS);
        return new BaseVo();
    }

}

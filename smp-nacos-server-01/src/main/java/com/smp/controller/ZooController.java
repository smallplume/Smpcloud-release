package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.enums.AnnimalTypeEnum;
import com.smp.server.IZooHandle;
import com.smp.server.ZooFactory;
import com.smp.vo.ZooParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 策略模式
 *
 * @author SmallPlume
 * @title: ZooController
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/13 14:11
 */
@RestController
@RequestMapping("/zoo")
public class ZooController {

    @Autowired
    private ZooFactory zooFactory;

    @PostMapping("/quark")
    public BaseVo quark(@RequestBody ZooParam zooParam) {
        IZooHandle zooHandle = zooFactory.getInstance(AnnimalTypeEnum.getByValue(zooParam.getAnnimalTypeEnum()));
        return new BaseVo(zooHandle.quark());
    }

}

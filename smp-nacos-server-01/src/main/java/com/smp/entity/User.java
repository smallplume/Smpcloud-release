package com.smp.entity;

import com.smp.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author SmallPlume
 * @title: User
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 16:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private Integer id;

    private String name;

    private Long balance;

    private Date upTime;

    public static User DTOc2Entity(UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        user.setBalance(userDTO.getBalance());
        user.setUpTime(new Date());
        return user;
    }
}

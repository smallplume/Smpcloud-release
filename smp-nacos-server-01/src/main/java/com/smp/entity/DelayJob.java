package com.smp.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author:359428217@qq.com
 * @date:2020/4/3 , 21:07
 * @version:1.0
 * @description: 延迟job
 **/
@Data
public class DelayJob implements Serializable {

    /**
     * job执行参数
     */
    private Map jobParams;

    /**
     * 具体执行实例实现
     */
    private Class aClass;

}

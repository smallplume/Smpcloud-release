package com.smp.support;

import com.smp.entity.DelayJob;
import com.smp.server.ExecuteJob;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author:359428217@qq.com
 * @date:2020/4/3 , 21:10
 * @version:1.0
 * @description:
 **/
@Component
public class JobTimer {

    public static final String jobsTag = "customer_jobtimer_jobs";

    @Autowired
    private RedissonClient client;

    @Autowired
    private ApplicationContext context;

    // Runtime.getRuntime().availableProcessors() * 2
    ExecutorService executorService = Executors.newFixedThreadPool(1);

    @PostConstruct
    public void startJobTimer() {
        RBlockingQueue blockingQueue = client.getBlockingQueue(jobsTag);
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        DelayJob job = (DelayJob) blockingQueue.take();
                        executorService.execute(new ExecutorTask(context, job));
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            TimeUnit.SECONDS.sleep(60);
                        } catch (Exception ex) {
                        }
                    }
                }
            }
        }.start();
    }
    class ExecutorTask implements Runnable {

        private ApplicationContext context;

        private DelayJob delayJob;

        public ExecutorTask(ApplicationContext context, DelayJob delayJob) {
            this.context = context;
            this.delayJob = delayJob;
        }

        @Override
        public void run() {
            ExecuteJob service = (ExecuteJob) context.getBean(delayJob.getAClass());
            service.execute(delayJob);
        }
    }

}

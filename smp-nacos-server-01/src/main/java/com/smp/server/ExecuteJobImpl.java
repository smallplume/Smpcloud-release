package com.smp.server;

import com.smp.entity.DelayJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * @author:359428217@qq.com
 * @date:2020/4/3 , 21:26
 * @version:1.0
 * @description:
 **/
@Slf4j
@Component
public class ExecuteJobImpl implements ExecuteJob {
    @Override
    public void execute(DelayJob job) {
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
        Iterator<Map.Entry<String, String>> iterator = job.getJobParams().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String key = entry.getKey();
            String value = entry.getValue();
            log.info("key : {}" , key);
            log.info("{} 消息的结束时间 >>>> {}", value, sdf.format(date));
        }
    }
}

package com.smp.server;

import com.google.common.collect.Maps;
import com.smp.enums.AnnimalTypeEnum;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Map;

/**
 * @author SmallPlume
 * @title: ZooFactory
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/13 14:02
 */
@Component
public class ZooFactory {

    private Map<AnnimalTypeEnum ,IZooHandle> map;

    @Resource
    private ApplicationContext applicationContext;

    @PostConstruct
    private void init() {
        Map<String, IZooHandle> beans = applicationContext.getBeansOfType(IZooHandle.class);
        map = Maps.newHashMapWithExpectedSize(beans.size());
        for (IZooHandle handle : beans.values()) {
            map.put(handle.supportedType(), handle);
        }
    }

    public IZooHandle getInstance(AnnimalTypeEnum typeEnum) {
        return map.getOrDefault(typeEnum, new DefaultAnnimal());
    }

    private final class DefaultAnnimal implements IZooHandle {
        @Override
        public String quark() {
            return "动物园未能找到对应的小动物，请选择捐赠！";
        }

        @Override
        public AnnimalTypeEnum supportedType() {
            return null;
        }
    }

}

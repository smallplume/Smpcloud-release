package com.smp.server;

import com.smp.dto.UserDTO;

/**
 * @author SmallPlume
 * @title: UserFacadeImpl
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 16:36
 */
public interface UserFacade {

    UserDTO inc(UserDTO userDTO);
}

package com.smp.server.impl;

import com.smp.enums.AnnimalTypeEnum;
import com.smp.server.IZooHandle;
import org.springframework.stereotype.Service;

/**
 * @author SmallPlume
 * @title: ZooCatImpl
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/13 14:08
 */
@Service
public class ZooCatImpl implements IZooHandle {
    @Override
    public String quark() {
        return "这是猫叫！！";
    }

    @Override
    public AnnimalTypeEnum supportedType() {
        return AnnimalTypeEnum.CAT;
    }
}

package com.smp.server.impl;

import com.smp.server.File;

import java.util.ArrayList;
import java.util.List;

/**
 * @author SmallPlume
 * @title: Folder
 * @projectName Smpcloud-release
 * @description: 组合模式
 * @date 2020/3/25 23:42
 */
public class Folder extends File {

    private List<File> files;

    public Folder(String name) {
        super(name);
        files = new ArrayList<File>();
    }

    @Override
    public void display() {
        for (File file : files) {
            file.display();
        }
    }

    /**
     * 添加文件
     * @param file
     */
    public void add(File file) {
        files.add(file);
    }

    /**
     * 移除文件
     * @param file
     */
    public void remove(File file) {
        files.remove(file);
    }

}

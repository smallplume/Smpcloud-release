package com.smp.server.impl;

import com.smp.server.File;

/**
 * @author SmallPlume
 * @title: TextFile
 * @projectName Smpcloud-release
 * @description: 组合模式
 * @date 2020/3/25 23:48
 */
public class TextFile extends File {

    public TextFile(String name) {
        super(name);
    }

    @Override
    public void display() {
        System.out.println("这是文本文件，文件名：" + super.getName());
    }
}

package com.smp.server.impl;

import com.smp.server.File;

/**
 * @author SmallPlume
 * @title: VideoFile
 * @projectName Smpcloud-release
 * @description: 组合模式
 * @date 2020/3/25 23:49
 */
public class VideoFile extends File {

    public VideoFile(String name) {
        super(name);
    }

    @Override
    public void display() {
        System.out.println("这是影像文件，文件名：" + super.getName());
    }
}

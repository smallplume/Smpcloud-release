package com.smp.server.impl;

import com.smp.enums.AnnimalTypeEnum;
import com.smp.server.IZooHandle;
import org.springframework.stereotype.Service;

/**
 * @author SmallPlume
 * @title: ZooDogImpl
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/13 14:09
 */
@Service
public class ZooDogImpl implements IZooHandle {
    @Override
    public String quark() {
        return "这是狗叫！！";
    }

    @Override
    public AnnimalTypeEnum supportedType() {
        return AnnimalTypeEnum.DOG;
    }
}

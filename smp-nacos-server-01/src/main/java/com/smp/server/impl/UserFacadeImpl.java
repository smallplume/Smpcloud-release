package com.smp.server.impl;

import com.smp.dao.UserMapper;
import com.smp.dto.UserDTO;
import com.smp.entity.User;
import com.smp.server.UserFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

/**
 * @author SmallPlume
 * @title: UserFacadeImpl
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 16:44
 */
@Slf4j
@Service
/*@RefreshScope*/
public class UserFacadeImpl implements UserFacade {

    @Autowired
    private UserMapper userMapper;

    @Value("${nacosTest:10086}")
    private String mysqlurl;

    @Override
    public UserDTO inc(UserDTO userDTO) {
        log.info("mysqlUrl >>>>>>>>>>>>>>>>>>>>> {}", mysqlurl);
        User user = User.DTOc2Entity(userDTO);
        userMapper.insert(user);
        userDTO.setId(user.getId());
        return userDTO;
    }
}

package com.smp.server.impl;

import com.smp.server.File;

/**
 * @author SmallPlume
 * @title: ImageFile
 * @projectName Smpcloud-release
 * @description: 组合模式
 * @date 2020/3/25 23:49
 */
public class ImageFile extends File {

    public ImageFile(String name) {
        super(name);
    }

    @Override
    public void display() {
        System.out.println("这是图像文件，文件名：" + super.getName());
    }
}

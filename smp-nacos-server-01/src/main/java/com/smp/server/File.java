package com.smp.server;

/**
 * @author SmallPlume
 * @title: File
 * @projectName Smpcloud-release
 * @description: 组合模式
 * @date 2020/3/25 23:41
 */
public abstract class File {

    private String name;

    public File(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void display();

}

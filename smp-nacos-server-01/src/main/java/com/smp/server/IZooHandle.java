package com.smp.server;

import com.smp.enums.AnnimalTypeEnum;

/**
 * @author SmallPlume
 * @title: IZooHandle
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/13 14:00
 */
public interface IZooHandle {

    /**
     * 叫声
     * @return
     */
    String quark();

    /**
     * 实现类支持的动物类型
     * @return
     */
    AnnimalTypeEnum supportedType();

}

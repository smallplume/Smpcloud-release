package com.smp.server;

import com.smp.entity.DelayJob;
import com.smp.support.JobTimer;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author:359428217@qq.com
 * @date:2020/4/3 , 21:12
 * @version:1.0
 * @description:
 **/
@Component
public class DelayJobService {

    @Autowired
    private RedissonClient client;

    public void submitJob(DelayJob job, Long delay, TimeUnit timeUnit){
        RBlockingQueue blockingQueue = client.getBlockingQueue(JobTimer.jobsTag);
        RDelayedQueue delayedQueue = client.getDelayedQueue(blockingQueue);
        delayedQueue.offer(job, delay, timeUnit);
    }

}

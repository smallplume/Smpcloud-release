package com.smp.server;

import com.smp.entity.DelayJob;

/**
 * @author:359428217@qq.com
 * @date:2020/4/3 , 21:09
 * @version:1.0
 * @description: 延迟job执行器
 **/
public interface ExecuteJob {

    void execute(DelayJob job);
}

package com.smp.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang.StringUtils;

/**
 * 策略模式
 * @author SmallPlume
 * @title: AnnimalTypeEnum
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/13 13:42
 */
public enum AnnimalTypeEnum {

    CAT("cat", "this is a cat"),
    DOG("dog", "this is a dog");

    private String value;
    private String desc;

    AnnimalTypeEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static AnnimalTypeEnum getByDesc(String desc) {
        for (AnnimalTypeEnum annimalTypeEnum : AnnimalTypeEnum.values()) {
            if (annimalTypeEnum.getDesc().equals(desc)) {
                return annimalTypeEnum;
            }
        }
        return null;
    }

    public static AnnimalTypeEnum getByValue(String value) {
        for (AnnimalTypeEnum annimalTypeEnum : AnnimalTypeEnum.values()) {
            if (StringUtils.equals(annimalTypeEnum.getValue(), value)) {
                return annimalTypeEnum;
            }
        }
        return null;
    }

    /**
     * 负责拿到传入controller传入枚举value参数的.删了(包括注解)系统直接瘫痪
     */
    @JsonValue
    public String getValue() {
        return value;
    }


    public String getDesc() {
        return desc;
    }

}

package com.smp.dao;

import com.smp.entity.User;

/**
 * @author SmallPlume
 * @title: UserMapper
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 16:32
 */
public interface UserMapper {

    void insert(User user);
}

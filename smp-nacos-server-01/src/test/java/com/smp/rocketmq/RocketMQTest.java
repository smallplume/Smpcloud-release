package com.smp.rocketmq;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

/**
 * @author SmallPlume
 * @title: RocketMQTest
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/16 12:56
 */
@Slf4j
public class RocketMQTest {

    private static String PRODUCE_RGROUP = "test_producer";

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer(PRODUCE_RGROUP);
        producer.setRetryTimesWhenSendFailed(3000);
        producer.setNamesrvAddr("127.0.0.1:9876");
        producer.start();

        Message message = new Message("topic_family", ("小小今年3岁").getBytes());
        // 发送 这里填写超时时间是5毫秒 所以每次都会发送失败
        SendResult sendResult = producer.send(message, 3000);
        log.info("输出生产者信息 = {}", sendResult);
    }
}

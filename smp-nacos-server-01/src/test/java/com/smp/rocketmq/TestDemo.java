package com.smp.rocketmq;

import lombok.Data;

/**
 * @author SmallPlume
 * @title: TestDemo
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/18 18:45
 */
public class TestDemo {

    public static void main(String[] args) {
        String str = "MDove";
        fun(str);
        System.out.println(str);
    }

    public static void fun(String str) {
        str = "MDone is cool.";
    }
}

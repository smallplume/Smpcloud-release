package com.smp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmpElasticsearchServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmpElasticsearchServerApplication.class, args);
    }

}

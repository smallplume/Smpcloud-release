package com.smp.constant;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/20
 * @version: v1.0
 * @description:
 **/
public class EsIndexConstant {

    public static final String USER_BEAN = "user_bean";

    public static final String BLOG_BEAN = "blog_bean";

    public static final String DOC_BEAN = "ems";
}

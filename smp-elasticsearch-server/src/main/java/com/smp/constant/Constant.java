package com.smp.constant;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/21
 * @version: v1.0
 * @description:
 **/
public class Constant {

    public static final String preTag = "<span style='color:red'>";

    public static final String postTag = "</span>";
}

package com.smp.service.impl;

import com.smp.dao.UserRepository;
import com.smp.entry.UserBean;
import com.smp.service.IUserService;
import com.smp.vo.PageInfo;
import com.smp.vo.UserAggretion;
import com.smp.vo.UserParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/17
 * @version: v1.0
 * @description:
 **/
@Slf4j
@Service("userService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(UserBean userBean) {
        userBean.setStatus(true);
        userBean.setCreateAt(System.currentTimeMillis());
        userRepository.save(userBean);
    }

    @Override
    public void update(UserBean userBean) {
        userRepository.update(userBean);
    }

    @Override
    public UserBean findById(String id) {
        return userRepository.findById(id).get();
    }

    @Override
    public Iterator<UserBean> findAll() {
        return userRepository.findAll().iterator();
    }

    @Override
    public PageInfo<UserBean> query(UserParam userParam) {
        return userRepository.query(userParam);
    }

    @Override
    public List<UserBean> queryUserBeanByBirthDay(String beginAt) {
        return userRepository.queryUserBeanByBirthDay(beginAt);
    }

    @Override
    public void deleteByName(String name) {
        userRepository.deleteByName(name);
    }

    @Override
    public UserAggretion queryByAggregations() {
        return userRepository.queryByAggregations();
    }

}

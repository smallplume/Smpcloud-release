package com.smp.service.impl;

import com.smp.dao.BlogRepository;
import com.smp.entry.BlogBean;
import com.smp.service.IBlogService;
import com.smp.vo.BlogParam;
import com.smp.vo.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/21
 * @version: v1.0
 * @description:
 **/
@Service("blogService")
public class BlogServiceImpl implements IBlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public void save(BlogBean blogBean) {
        blogBean.setCreateAt(System.currentTimeMillis());
        blogBean.setStatus(1);
        blogRepository.save(blogBean);
    }

    @Override
    public List<BlogBean> queryByTags(String tag) {
        return blogRepository.queryByTags(tag);
    }

    @Override
    public PageInfo<BlogBean> query(BlogParam param) {
        return blogRepository.query(param);
    }

    @Override
    public List<BlogBean> queryNest(String name) {
        return blogRepository.queryNest(name);
    }

}

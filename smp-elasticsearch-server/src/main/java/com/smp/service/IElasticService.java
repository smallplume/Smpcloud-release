package com.smp.service;

import com.smp.entry.DocBean;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.SearchHits;

import java.util.Iterator;
import java.util.List;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/13
 * @version: v1.0
 * @description:
 **/
public interface IElasticService {

    void save(DocBean docBean);

    void saveAll(List<DocBean> list);

    Iterator<DocBean> findAll();

    DocBean findByContent(String content);

    Page<DocBean> findByFirstCode(String firstCode);

    SearchHits<DocBean> query(DocBean docBean);

}

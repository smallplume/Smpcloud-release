package com.smp.service;

import com.smp.entry.UserBean;
import com.smp.vo.PageInfo;
import com.smp.vo.UserAggretion;
import com.smp.vo.UserParam;
import org.elasticsearch.search.aggregations.Aggregation;
import org.springframework.data.elasticsearch.core.SearchHit;

import java.util.Iterator;
import java.util.List;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/17
 * @version: v1.0
 * @description:
 **/
public interface IUserService {

    void save(UserBean userBean);

    void update(UserBean userBean);

    UserBean findById(String id);

    Iterator<UserBean> findAll();

    PageInfo<UserBean> query(UserParam userParam);

    List<UserBean> queryUserBeanByBirthDay(String beginAt);

    void deleteByName(String name);

    UserAggretion queryByAggregations();

}

package com.smp.service;

import com.smp.entry.BlogBean;
import com.smp.vo.BlogParam;
import com.smp.vo.PageInfo;

import java.util.List;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/21
 * @version: v1.0
 * @description:
 **/
public interface IBlogService {

    void save(BlogBean blogBean);

    List<BlogBean> queryByTags(String tag);

    PageInfo<BlogBean> query(BlogParam param);

    /**
     * 嵌套查询
     *
     * @param name
     * @return
     */
    List<BlogBean> queryNest(String name);

}

package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.entry.UserBean;
import com.smp.service.IUserService;
import com.smp.vo.UserParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/17
 * @version: v1.0
 * @description:
 **/
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping("/save")
    public BaseVo save(@RequestBody UserBean userBean) {
        userService.save(userBean);
        return new BaseVo();
    }

    @PostMapping("/update")
    public BaseVo update(@RequestBody UserBean userBean) {
        userService.update(userBean);
        return new BaseVo();
    }

    @GetMapping("/findById")
    public BaseVo findById(@RequestParam("id") String id) {
        return new BaseVo(userService.findById(id));
    }

    @GetMapping("/findAll")
    public BaseVo findAll() {
        return new BaseVo(userService.findAll());
    }

    @GetMapping("/queryByBirthDay")
    public BaseVo queryByBirthDay(@RequestParam("birthDay") String birthDay) {
        return new BaseVo(userService.queryUserBeanByBirthDay(birthDay));
    }

    @PostMapping("/query")
    public BaseVo query(@RequestBody UserParam param) {
        return new BaseVo(userService.query(param));
    }

    @GetMapping("/deleteByName")
    public BaseVo deleteByName(@RequestParam("name") String name) {
        userService.deleteByName(name);
        return new BaseVo();
    }

    @GetMapping("/aggregations")
    public BaseVo queryByAggregations() {
        return new BaseVo(userService.queryByAggregations());
    }

}

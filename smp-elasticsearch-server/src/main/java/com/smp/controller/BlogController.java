package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.entry.BlogBean;
import com.smp.service.IBlogService;
import com.smp.vo.BlogParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/21
 * @version: v1.0
 * @description:
 **/
@RestController
@RequestMapping("/blog")
public class BlogController {

    @Autowired
    private IBlogService blogService;

    @RequestMapping("/save")
    public BaseVo save(@RequestBody BlogBean blogBean) {
        blogService.save(blogBean);
        return new BaseVo();
    }

    @GetMapping("/queryByTags")
    public BaseVo queryByTags(@RequestParam("tag") String tag) {
        return new BaseVo(blogService.queryByTags(tag));
    }

    @PostMapping("/query")
    public BaseVo query(@RequestBody BlogParam blogParam) {
        return new BaseVo(blogService.query(blogParam));
    }

    @GetMapping("/queryNest")
    public BaseVo queryNest(@RequestParam("name") String name) {
        return new BaseVo(blogService.queryNest(name));
    }

}

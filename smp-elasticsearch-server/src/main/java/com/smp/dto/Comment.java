package com.smp.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/21
 * @version: v1.0
 * @description:
 **/
@Data
public class Comment {

    private String id;

    private String name;

    private String remark;

    private Date createAt;

}

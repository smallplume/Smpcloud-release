package com.smp.entry;

import com.smp.constant.EsIndexConstant;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/13
 * @version: v1.0
 * @description:
 **/
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Document(indexName = EsIndexConstant.DOC_BEAN, shards = 5, replicas = 1)
public class DocBean implements Serializable {

    @Id
    private String id;

    @Field(type = FieldType.Keyword)
    private String firstCode;

    @Field(type = FieldType.Text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String content;

    @Field(type = FieldType.Integer)
    private Integer type;

    @Field(type = FieldType.Keyword)
    private String name;

    public DocBean(String firstCode, String content, Integer type, String name) {
        this.firstCode = firstCode;
        this.content = content;
        this.type = type;
        this.name = name;
    }

}

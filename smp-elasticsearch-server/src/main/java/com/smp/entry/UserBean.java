package com.smp.entry;

import com.smp.constant.EsIndexConstant;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/17
 * @version: v1.0
 * @description:
 **/
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Document(indexName = EsIndexConstant.USER_BEAN, shards = 5, replicas = 1)
public class UserBean implements Serializable {

    @Id
    private String id;

    @Field(type = FieldType.Keyword)
    private String name;

    private Integer sex;

    @Field(type = FieldType.Date, format = DateFormat.date)
    private LocalDate birthDay;

    private Long createAt;

    /**
     * true：有效
     * false：无效
     */
    private Boolean status;

}

package com.smp.entry;

import com.smp.constant.EsIndexConstant;
import com.smp.dto.Comment;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/21
 * @version: v1.0
 * @description:
 **/
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Document(indexName = EsIndexConstant.BLOG_BEAN, shards = 5, replicas = 1)
public class BlogBean {

    @Id
    private String id;

    @Field(type = FieldType.Text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String title;

    @Field(type = FieldType.Text, analyzer = "ik_smart", searchAnalyzer = "ik_smart")
    private String content;

    @Field(type = FieldType.Text, analyzer = "ik_smart", searchAnalyzer = "ik_smart")
    private String author;

    @Field(type = FieldType.Long)
    private Long createAt;

    /**
     * 标签
     */
    @Field(type = FieldType.Keyword)
    private String[] tags;

    /**
     * 评论
     */
    @Field(type = FieldType.Object)
    private List<Comment> comments;

    @Field(type = FieldType.Integer)
    private Integer status;

}

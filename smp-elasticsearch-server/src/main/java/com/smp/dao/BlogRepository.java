package com.smp.dao;

import com.smp.entry.BlogBean;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/21
 * @version: v1.0
 * @description:
 **/
@Component
public interface BlogRepository extends ElasticsearchRepository<BlogBean, String>, IBlogRepository<BlogBean, String> {

}

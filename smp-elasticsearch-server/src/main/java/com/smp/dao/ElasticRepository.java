package com.smp.dao;

import com.smp.entry.DocBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/13
 * @version: v1.0
 * @description:
 **/
@Component
public interface ElasticRepository extends ElasticsearchRepository<DocBean, String> {

    DocBean findByContent(String content);

    Page<DocBean> findByFirstCode(String firstCode, Pageable pageable);

}

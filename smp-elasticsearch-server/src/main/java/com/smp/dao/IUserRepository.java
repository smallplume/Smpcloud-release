package com.smp.dao;

import com.smp.entry.UserBean;
import com.smp.vo.PageInfo;
import com.smp.vo.UserAggretion;
import com.smp.vo.UserParam;

import java.io.Serializable;
import java.util.List;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/25
 * @version: v1.0
 * @description:
 **/
public interface IUserRepository<T, ID extends Serializable> {

    void update(UserBean userBean);

    PageInfo<UserBean> query(UserParam userParam);

    List<UserBean> queryUserBeanByBirthDay(String beginAt);

    UserAggretion queryByAggregations();

}

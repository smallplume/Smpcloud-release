package com.smp.dao;

import com.smp.entry.UserBean;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/17
 * @version: v1.0
 * @description:
 **/
@Component
public interface UserRepository extends ElasticsearchRepository<UserBean, String>, IUserRepository<UserBean, String> {

    UserBean findByName(String name);

    void deleteByName(String name);

}

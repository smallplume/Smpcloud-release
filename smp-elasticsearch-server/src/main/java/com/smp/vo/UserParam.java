package com.smp.vo;

import lombok.Data;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/19
 * @version: v1.0
 * @description:
 **/
@Data
public class UserParam extends PageParam {

    private String name;

}

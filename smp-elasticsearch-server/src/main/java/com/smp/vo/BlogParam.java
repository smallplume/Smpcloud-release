package com.smp.vo;

import lombok.Data;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/21
 * @version: v1.0
 * @description:
 **/
@Data
public class BlogParam extends PageParam {

    private String keyword;
}

package com.smp.vo;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/16
 * @version: v1.0
 * @description:
 **/
@Data
public class PageInfo<T> implements Serializable {

    private Integer pageSize;

    private Integer pageNum;

    private Integer total;

    private List<T> data;

    private PageInfo() {
    }

    public PageInfo(Page page) {
        this.pageSize = page.getSize();
        this.pageNum = page.getNumber();
        this.total = page.getTotalPages();
        this.data = page.getContent();
    }

    public PageInfo(Integer pageNum, Integer pageSize, Integer total, List data) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.total = total;
        this.data = data;
    }

}

package com.smp.vo;

import lombok.Data;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/19
 * @version: v1.0
 * @description:
 **/
@Data
public class PageParam {

    private Integer pageNum = 1;

    private Integer pageSize = 10;

}

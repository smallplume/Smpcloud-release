package com.smp.vo;

import lombok.Data;

import java.util.Map;

/**
 * @author: xiaoyu.fang
 * @date: 2020/8/20
 * @version: v1.0
 * @description:
 **/
@Data
public class UserAggretion {

    private String name;

    private Map<String, Object> stats;

}

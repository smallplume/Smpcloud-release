package com.smp.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: fangxiaoyu
 * @date: 2021/7/2
 * @description:
 **/
public class ThreadContextHolder {

    public static final ThreadLocal<Map<String, Object>> context = new MyThreadLocal();

    private static class MyThreadLocal extends ThreadLocal<Map<String, Object>> {
        @Override
        protected Map<String, Object> initialValue() {
            return new HashMap<>();
        }
    }

    public static Object get(String key) {
        return getMap().get(key);
    }

    private static Map<String, Object> getMap() {
        return context.get();
    }

    public static void put(String k, Object v) {
        getMap().put(k, v);
    }

    public static void reset() {
        getMap().clear();
    }
}

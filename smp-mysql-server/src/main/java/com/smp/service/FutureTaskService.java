package com.smp.service;

import java.util.concurrent.ExecutionException;

/**
 * @author: fangxiaoyu
 * @date: 2021/7/1
 * @description:
 **/
public interface FutureTaskService {

    String eatTea() throws ExecutionException, InterruptedException;

    String eatTea2() throws InterruptedException;

    String eatTea3() throws InterruptedException;

}

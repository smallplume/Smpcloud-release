package com.smp.service;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/29
 * @description:
 **/
public interface FruitService {

    /**
     * 品种
     *
     * @return
     */
    String breed();

}

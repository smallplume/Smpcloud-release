package com.smp.service.impl;

import com.smp.service.FruitService;
import org.springframework.stereotype.Service;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/29
 * @description:
 **/
@Service("pear")
public class PearServiceImpl implements FruitService {
    @Override
    public String breed() {
        return "鸭梨";
    }
}

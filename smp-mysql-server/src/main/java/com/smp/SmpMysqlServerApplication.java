package com.smp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmpMysqlServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmpMysqlServerApplication.class, args);
	}

}

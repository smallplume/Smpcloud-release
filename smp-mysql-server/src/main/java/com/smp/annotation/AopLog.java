package com.smp.annotation;

import java.lang.annotation.*;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/30
 * @description:
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AopLog {
}

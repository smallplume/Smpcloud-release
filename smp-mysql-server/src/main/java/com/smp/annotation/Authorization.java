package com.smp.annotation;

/**
 * @author: fangxiaoyu
 * @date: 2020/4/26 ,23:38
 * @version: v1.0
 * @description:
 **/

import java.lang.annotation.*;

@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface Authorization {

    AuthCode value() default AuthCode.ALLOW;

    enum AuthCode {
        ALLOW(0, "", "允许访问"),
        VERIFY(1, "session","需要校验");
        /**
         * 权限标识
         */
        private int authId;
        /**
         * 权限编码
         */
        private String authCode;
        /**
         * 权限名称
         */
        private String authName;

        AuthCode(int authId, String authCode, String authName) {
            this.authId = authId;
            this.authCode = authCode;
            this.authName = authName;
        }
    }

}

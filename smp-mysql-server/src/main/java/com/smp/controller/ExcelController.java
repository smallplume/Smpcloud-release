package com.smp.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.smp.entity.LevelData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/25
 * @description:
 **/
@Slf4j
@RestController
@RequestMapping("/excel")
public class ExcelController {

    @PostMapping("/upload")
    public void importExcel(@RequestParam("file") MultipartFile file) {
        List<LevelData> levelDataList = new ArrayList<>();
        try {
            EasyExcel.read(file.getInputStream(), LevelData.class, new AnalysisEventListener<LevelData>() {
                @Override
                public void invoke(LevelData levelData, AnalysisContext analysisContext) {
                    levelDataList.add(levelData);
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                    log.info("levelList json >>>>> {}", levelDataList);
                }
            }).sheet().headRowNumber(3).doRead();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

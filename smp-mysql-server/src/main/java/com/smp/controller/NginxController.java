package com.smp.controller;

import com.smp.common.BaseVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/23
 * @description:
 **/
@RestController
@RequestMapping("/nginx")
public class NginxController {

    @GetMapping("/getInfo")
    public BaseVo getInfo() {
        return new BaseVo("Nginx 8092");
    }

}

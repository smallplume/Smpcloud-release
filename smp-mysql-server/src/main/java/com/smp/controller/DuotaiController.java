package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.service.FruitService;
import com.smp.service.FutureTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/29
 * @description:
 **/
@RestController
@RequestMapping("/fruit")
public class DuotaiController {

    @Autowired
    private Map<String, FruitService> fruitServiceMap;

    @Autowired
    private List<FruitService> fruitServices;

    @Autowired
    private FutureTaskService futureTaskService;

    @GetMapping("/breed")
    public BaseVo getBreed(@RequestParam("breed") String breed) {
        FruitService fruitService = fruitServiceMap.get(breed);
        fruitServices.stream().forEach(fruitServ -> System.out.println(fruitServ.breed()));
        return new BaseVo(fruitService.breed());
    }

    @GetMapping("/eatTea")
    public BaseVo eatTea() throws ExecutionException, InterruptedException {
        return new BaseVo(futureTaskService.eatTea());
    }

    @GetMapping("/eatTea2")
    public BaseVo eatTea2() throws InterruptedException {
        return new BaseVo(futureTaskService.eatTea2());
    }

    @GetMapping("/eatTea3")
    public BaseVo eatTea3() throws InterruptedException {
        return new BaseVo(futureTaskService.eatTea3());
    }

}

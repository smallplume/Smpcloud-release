package com.smp.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.smp.annotation.AopLog;
import com.smp.annotation.Authorization;
import com.smp.common.BaseVo;
import com.smp.entity.User;
import com.smp.entity.WrapBaseVO;
import com.smp.mapper.UserMapper;
import com.smp.utils.ThreadContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/24
 * @description:
 **/
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserMapper userMapper;

    @Authorization
    @GetMapping("/query")
    public BaseVo queryUser(User user) {
        log.info("user = {}", user.toString());
        IPage<User> page = new Page<>(1, 100);
        IPage<User> userListPage = userMapper.selectPage(page, new LambdaQueryWrapper<>());
        return new BaseVo(userListPage);
    }

    @AopLog
    @Authorization
    @GetMapping("/getInfo")
    public BaseVo getInfo(User user) {
        log.info("controller本地线程名称：{}",Thread.currentThread().getName());
        MDC.put("abc", "123456");
        ThreadContextHolder.put("123", "10086");
        return WrapBaseVO.create(new BaseVo(user));
    }

}

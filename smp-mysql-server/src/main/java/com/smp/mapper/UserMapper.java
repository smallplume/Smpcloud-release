package com.smp.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.smp.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/23
 * @description:
 **/
public interface UserMapper extends BaseMapper<User> {

    @Select("select * from user ${ew.customSqlSegment}")
    List<User> getAll1(@Param(Constants.WRAPPER) LambdaQueryWrapper lambdaQueryWrapper);

    List<User> getAll2(IPage<User> page);

    @Select("select * from user where id = #{id}")
    List<User> getAll3(Long id);

    @Select("select * from user ${ew.customSqlSegment}")
    List<User> getAll4(@Param(Constants.WRAPPER) LambdaQueryWrapper lambdaQueryWrapper);

}

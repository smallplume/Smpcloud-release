package com.smp.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/25
 * @description:
 **/
@Data
@ToString
public class LevelData implements Serializable {

    @ExcelProperty(value = {"中文版本", "一级"}, index = 1)
    private String level1;

    @ExcelProperty(value = {"中文版本", "二级"}, index = 2)
    private String level2;

    @ExcelProperty(value = {"中文版本", "三级"}, index = 3)
    private String level3;

    @ExcelProperty(value = {"中文版本", "四级"}, index = 4)
    private String level4;

    @ExcelProperty(value = {"中文版本", "五级"}, index = 5)
    private String level5;

}

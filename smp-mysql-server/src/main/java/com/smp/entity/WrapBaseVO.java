package com.smp.entity;

import com.smp.common.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/30
 * @description:
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class WrapBaseVO extends BaseVo {

    private BaseVo baseVo;

    public WrapBaseVO(BaseVo baseVo) {
        this.baseVo = baseVo;
    }

    public static WrapBaseVO create(BaseVo baseVo) {
        return new WrapBaseVO(baseVo);
    }

    public BaseVo getBaseVo() {
        return baseVo;
    }
}

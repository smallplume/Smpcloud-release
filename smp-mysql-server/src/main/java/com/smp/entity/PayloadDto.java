package com.smp.entity;

import lombok.Data;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/30
 * @description:
 **/
@Data
public class PayloadDto {

    private Long id;

    private String name;

    private Long exp;
}

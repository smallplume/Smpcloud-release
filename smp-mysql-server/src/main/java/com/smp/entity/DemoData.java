package com.smp.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/24
 * @description:
 **/
@Data
@HeadRowHeight(value = 20) // 表头行高
@ContentRowHeight(value = 20) // 内容行高
// @ColumnWidth(value = 20) // 列宽
public class DemoData {

    /**
     * 设置excel表头名称
     */
    @ExcelProperty(value = {"学生信息","学生编号"}, index = 0)
    private Integer sno;

    @ExcelProperty(value = {"学生信息","学生姓名"}, index = 1)
    private String sname;

}

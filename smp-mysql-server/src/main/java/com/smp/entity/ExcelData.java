package com.smp.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.net.URL;

/**
 * @author: fangxiaoyu
 * @date: 2021/7/5
 * @description:
 **/
@Data
@HeadRowHeight(value = 20) // 表头行高
@ContentRowHeight(value = 30) // 内容行高
// @ColumnWidth(value = 25) // 列宽
public class ExcelData {

    @ExcelProperty(value = {"名称"}, index = 0)
    private String name;

    @ColumnWidth(20)
    @ExcelProperty(value = {"LOGO"}, index = 1)
    private URL url;

}

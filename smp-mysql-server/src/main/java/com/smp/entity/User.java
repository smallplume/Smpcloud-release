package com.smp.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/23
 * @description:
 **/
@Data
@ToString
public class User {

    private Long id;
    private String name;
    private Integer age;
    private String email;

}

package com.smp.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/25
 * @description:
 **/
public enum LevelTypeEnum {

    LEVEL1("level1", "一级类目"),
    LEVEL2("level2", "二级类目"),
    LEVEL3("level3", "三级类目"),
    LEVEL4("level4", "四级类目"),
    LEVEL5("level5", "五级类目");

    private String type;

    private String desc;

    private static Map<String, LevelTypeEnum> map = new HashMap<>(5);

    static {
        for (LevelTypeEnum levelTypeEnum : LevelTypeEnum.values()) {
            map.put(levelTypeEnum.getType(), levelTypeEnum);
        }
    }

    LevelTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static LevelTypeEnum getByType(String type) {
        return map.getOrDefault(type, null);
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}

package com.smp.config;

import com.smp.filter.InvocationContextMethodArgumentResolver;
import com.smp.filter.TokenInterceptor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/30
 * @description:
 **/
@Configuration
@EnableAutoConfiguration
public class WebExceptionFilter extends WebMvcConfigurationSupport {

    @Bean
    public HandlerInterceptor getTokenInterceptor() {
        return new TokenInterceptor();
    }

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getTokenInterceptor())
                // 拦截所有
                .addPathPatterns("/**")
        // 排除拦截URL
        .excludePathPatterns("/fruit/**");
        super.addInterceptors(registry);
    }

    @Override
    protected void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new InvocationContextMethodArgumentResolver());
    }
}

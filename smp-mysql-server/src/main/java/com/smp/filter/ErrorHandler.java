//package com.smp.filter;
//
//import com.aliyun.citybrain.common.exceptions.BusinessException;
//import com.aliyun.citybrain.common.exceptions.ErrCheckException;
//import com.aliyun.citybrain.enums.ResultCode;
//import com.aliyun.citybrain.utils.AjaxResult;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Component;
//import org.springframework.validation.BindException;
//import org.springframework.web.bind.MethodArgumentNotValidException;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.ResponseStatus;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * @author: xiaoyu
// * @date: 2020/4/24 ,13:31
// * @version: v1.0
// * @description: 业务异常，参数校验异常等拦截
// **/
//@Slf4j
//@Component
//@ControllerAdvice
//public class ErrorHandler {
//
//    /**
//     * 业务异常
//     *
//     * @param ex
//     * @return
//     */
//    @ExceptionHandler(BusinessException.class)
//    @ResponseBody
//    @ResponseStatus(value = HttpStatus.OK)
//    public AjaxResult handleBindException(BusinessException ex) {
//        log.error(ex.getMessage(), ex);
//        return AjaxResult.error(ex.getStatusCode() == null ? ResultCode.WRONG.getCode() : ex.getStatusCode(), ex.getMessage());
//    }
//
//    /**
//     * 参数验证、绑定异常
//     *
//     * @param ex
//     * @return
//     */
//    @ResponseBody
//    @ResponseStatus(value = HttpStatus.OK)
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public AjaxResult handleValidateException(MethodArgumentNotValidException ex) {
//        log.error(ex.getMessage(), ex);
//        return AjaxResult.error(ResultCode.ERR_CHECK_PARAM.getCode(), ex.getBindingResult().getFieldError().getDefaultMessage());
//    }
//
//    /**
//     * 业务校验异常,前端提示校验信息
//     *
//     * @param ex
//     * @return
//     * @date 2020-08-03
//     */
//    @ExceptionHandler(ErrCheckException.class)
//    @ResponseBody
//    @ResponseStatus(value = HttpStatus.OK)
//    public AjaxResult handleErrCheckException(ErrCheckException ex) {
//        log.error(ex.getMessage(), ex);
//        return AjaxResult.error(ex.getStatusCode() == null ? ResultCode.ERR_CHECK_PARAM.getCode() : ex.getStatusCode(), ex.getMessage());
//    }
//
//    /**
//     * 控制层参数校验
//     *
//     * @param ex
//     * @param req
//     * @param res
//     * @return
//     */
//    @ResponseBody
//    @ExceptionHandler(Throwable.class)
//    @ResponseStatus(value = HttpStatus.OK)
//    public AjaxResult handleAllException(Throwable ex, HttpServletRequest req, HttpServletResponse res) {
//        log.error(ex.getMessage(), ex);
//        return AjaxResult.error(ResultCode.WRONG.getCode(), ResultCode.WRONG.getInfo());
//    }
//
//    /**
//     * 绑定异常
//     *
//     * @param ex
//     * @return
//     */
//    @ExceptionHandler(BindException.class)
//    @ResponseBody
//    @ResponseStatus(value = HttpStatus.OK)
//    public AjaxResult handleBindException(BindException ex) {
//        log.error(ex.getMessage(), ex);
//        return AjaxResult.error(ResultCode.PARAMETER_ERROR.getCode(), ex.getFieldError().getDefaultMessage());
//    }
//
//}

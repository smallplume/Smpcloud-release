package com.smp.support;

import com.smp.annotation.AopLog;
import com.smp.entity.WrapBaseVO;
import com.smp.utils.ThreadContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/30
 * @description:
 **/
@Aspect
@Component
@Slf4j
public class LogAspect implements Ordered {

    @Around(value = "@annotation(aopLog)")
    public Object doAround(ProceedingJoinPoint pjp, AopLog aopLog) throws Throwable {
        // 获取返回对象BaseVO
        Object obj = pjp.proceed();
        log.info("aop本地线程名称：{}",Thread.currentThread().getName());
        String abc = MDC.get("abc");
        log.info("abc：{}", abc);
        MDC.remove("abc");

        String str = (String) ThreadContextHolder.get("123");
        log.info("str：{}",str);
        ThreadContextHolder.reset();
        return unwrapResult(obj);
    }

    private Object unwrapResult(Object result) {
        if (result instanceof WrapBaseVO) {
            return ((WrapBaseVO) result).getBaseVo();
        }
        return result;
    }

    @Override
    public int getOrder() {
        return 1;
    }
}

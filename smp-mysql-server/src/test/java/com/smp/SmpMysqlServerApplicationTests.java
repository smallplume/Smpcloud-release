package com.smp;

import cn.hutool.extra.emoji.EmojiUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.smp.entity.User;
import com.smp.mapper.UserMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@SpringBootTest
class SmpMysqlServerApplicationTests {

    @Resource
    private UserMapper userMapper;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        List<User> userList = userMapper.selectList(null);
        Assert.assertEquals(5, userList.size());
        userList.forEach(System.out::println);
    }

    @Test
    public void testGetAll1() {
        System.out.println("----- getAll1 method test -----");
        List<User> userList = userMapper.getAll1(new LambdaQueryWrapper<User>().eq(User::getId, 1));
        userList.forEach(System.out::println);
    }

    @Test
    public void testGetAll2() {
        System.out.println("----- getAll2 method test -----");
        IPage<User> page = new Page<>(1, 3);
        List<User> userList = userMapper.getAll2(page);
        userList.forEach(System.out::println);
    }

    @Test
    public void testGetAll3() {
        System.out.println("----- getAll3 method test -----");
        List<User> userList = userMapper.getAll3(3L);
        userList.forEach(System.out::println);
    }

    @Test
    public void testGetAll4() {
        System.out.println("----- getAll4 method test -----");
        List<User> userList = userMapper.getAll4(new LambdaQueryWrapper<User>().in(User::getId, Arrays.asList(1, 2, 5)));
        userList.forEach(System.out::println);
    }

    @Test
    public void testPage5() {
        System.out.println("----- Page method test -----");
        IPage<User> page = new Page<>(1, 10);
        IPage<User> userListPage = userMapper.selectPage(page, new LambdaQueryWrapper<>());
        userListPage.getRecords().forEach(System.out::println);

        System.out.println("---------- json -----------");
        IPage page2 = new Page<>(1, 10);
        IPage usersPage = userMapper.selectMapsPage(page2, new QueryWrapper<>());
        usersPage.getRecords().forEach(System.out::println);
    }

    @Test
    public void testInsert6() {
        String alias = EmojiUtil.toAlias("哈哈哈哈😄");//:smile:

        // String emoji = EmojiUtil.toUnicode(":smile:");//😄

        String emoji = EmojiUtil.toUnicode(alias);//😄

        //String alias2 = EmojiUtil.toHtml("😄");//&#128102;

        User user = new User();
        user.setName(emoji);
        user.setAge(11);
        user.setEmail("abc@123.com");
        userMapper.insert(user);
    }


    private static final Logger logger = LoggerFactory.getLogger(SmpMysqlServerApplicationTests.class);
    public static final String REQ_ID = "REQ_ID";

    public static void main(String[] args) {
        MDC.put(REQ_ID, UUID.randomUUID().toString());
        logger.info("开始调用服务A，进行业务处理");
        logger.info("业务处理完毕，可以释放空间了，避免内存泄露");
        logger.info(" ====== {}", MDC.get(REQ_ID));
        MDC.remove(REQ_ID);
        logger.info("REQ_ID 还有吗？{}", MDC.get(REQ_ID) != null);
    }

}

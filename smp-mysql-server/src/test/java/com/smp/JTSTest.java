package com.smp;

import com.nimbusds.jose.JOSEException;
import com.smp.utils.TokenUtils;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/30
 * @description:
 **/
public class JTSTest {

    @Test
    public void test1() throws JOSEException {
        Map<String, Object> params = new HashMap<>();
        params.put("name", "张三");
        params.put("id", 10086L);
        params.put("exp", System.currentTimeMillis());
        String tokenHS256 = TokenUtils.createTokenHS256(params);
        System.out.println(tokenHS256);

        TokenUtils.TokenResult tokenResult = TokenUtils.validRS256(tokenHS256);
        System.out.println(tokenResult);
    }

}

package com.smp;

import org.junit.Test;
import org.slf4j.MDC;

/**
 * @author: fangxiaoyu
 * @date: 2021/7/1
 * @description:
 **/
public class MDCTest {

    @Test
    public void test1() {
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            new Thread(()->{
                MDC.put("abc", finalI + "");
                String str = MDC.get("abc");
                System.out.println(" >>> " + str);
                MDC.clear();
            }).start();
        }
        String abc = MDC.get("abc");
        System.out.println(abc);
    }

}

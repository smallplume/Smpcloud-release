package com.smp;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import com.smp.entity.DemoData;
import com.smp.entity.ExcelData;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/24
 * @description:
 **/
@SpringBootTest
public class ExcelTest {

    @Test
    public void excelTest1() {
        //实现excel写的操作
        //1 设置写入文件夹地址和excel文件名称
        String filename = "E:\\write.xlsx";
        // 2 调用easyexcel里面的方法实现写操作
        // write方法两个参数：第一个参数文件路径名称，第二个参数实体类class
        EasyExcel.write(filename, DemoData.class).registerWriteHandler(new LongestMatchColumnWidthStyleStrategy()).sheet("学生列表").doWrite(getData());
    }

    @Test
    public void test2() throws IOException {
        String fileName = "E:\\" + System.currentTimeMillis() + ".xlsx";
        // 如果使用流 记得关闭
        InputStream inputStream = null;
        try {
            List<ExcelData> list = new ArrayList<>();
            ExcelData imageData = new ExcelData();
            list.add(imageData);
            imageData.setName(System.currentTimeMillis() + "");
            imageData.setUrl(new URL(
                    "https://proseven.digitalexpo.com/expo/default/exhibitor/group/20201217100251012_g3v4sud8.jpeg"));
            EasyExcel.write(fileName, ExcelData.class).sheet().doWrite(list);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private List<DemoData> getData() {
        List<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DemoData data = new DemoData();
            data.setSno(i);
            data.setSname("lucy" + i);
            list.add(data);
        }
        return list;
    }

    public static void main(String[] args) {

    }

}

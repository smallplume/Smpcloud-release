package com.smp;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.pingplusplus.exception.ChannelException;
import com.pingplusplus.util.WxLiteOAuth;
import com.smp.domain.BaseInputDomain;
import com.smp.domain.BaseOutputDomain;
import com.smp.domain.SmpQueryResult;
import com.smp.enums.ServiceTypeEnum;
import com.smp.exception.ExpoAppException;
import com.smp.util.PicBaseUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/16
 * @description:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class SaomaTest {

    @Test
    public void test1() {
        String url = "http://sissi.pingxx.com/mock.php?ch_id=ch_1aDuv98Gy5S8fP8GKO4Wb1GG&channel=alipay_qr";
        String base = (new PicBaseUtils()).getBase64(url);
        String base64 = "data:image/png;base64," + base;
        System.out.println(base64);
    }

    @Test
    public void test2() {
        System.out.println(DateUtil.currentSeconds());
        String dateStr = "2017-03-01 22:33:23";
        Date date = DateUtil.parse(dateStr);
        System.out.println(DateUtil.offset(date, DateField.SECOND, 60));
    }

    @Test
    public void test3() throws UnsupportedEncodingException, ChannelException {
        String openId = WxLiteOAuth.getOpenId("", "", "");
        System.out.println(openId);
    }

    // @Test
    public void test4(String authCode) throws AlipayApiException {
        //使用支付宝小程序的固定方法获取auth_code
        // https://opendocs.alipay.com/open/54/103419
        if (StringUtils.isBlank(authCode)) {
            System.out.println("请求参数auth_code不能为空");
            return;
        } else {
            //String serverUrl, String appId, String privateKey, String format,String charset, String alipayPublicKey, String signType
            //实例化客户端 参数：正式环境URL,Appid,商户私钥 PKCS8格式,字符编码格式,字符格式,支付宝公钥,签名方式
            AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", "", "", "json", "GBK", "", "RSA2");
            AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
            // 值为authorization_code时，代表用code换取
            request.setGrantType("authorization_code");
            //授权码，用户对应用授权后得到的
            request.setCode(authCode);
            //这里使用execute方法
            AlipaySystemOauthTokenResponse response = alipayClient.execute(request);
            //刷新令牌，上次换取访问令牌时得到。见出参的refresh_token字段
            request.setRefreshToken(response.getAccessToken());
            //返回成功时 就将唯一标识返回
            if (response.isSuccess()) {
                System.out.println("调用成功");
                //我这里只返回了一个字段给前端用
                String userId = response.getUserId();
                System.out.println(userId);
            } else {
                System.out.println("调用失败");
            }
        }
    }
}

package com.smp.thread;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @author: fangxiaoyu
 * @date: 2021/7/1
 * @description:
 **/
@Slf4j
public class MakeTeaExampleTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = new ThreadPoolExecutor(2, 2, 50, TimeUnit.SECONDS, new LinkedBlockingDeque<>());

        FutureTask<String> t2Task = new FutureTask<>(new T2Task());
        FutureTask<String> t1Task = new FutureTask<>(new T1Task(t2Task));

        String beginTime = DateUtil.now();
        executorService.submit(t1Task);
        executorService.submit(t2Task);

        log.info(t1Task.get());
        String endTime = DateUtil.now();
        log.info("beginTime：{}，endTime：{}", beginTime, endTime);
        executorService.shutdown();
    }

    static class T1Task implements Callable<String> {

        private FutureTask<String> t2Task;

        public T1Task(FutureTask<String> t2Task) {
            this.t2Task = t2Task;
        }

        @Override
        public String call() throws Exception {
            log.info("T1：洗茶壶...");
            TimeUnit.SECONDS.sleep(1);

            log.info("T1：烧开水...");
            TimeUnit.SECONDS.sleep(2);

            while (!t2Task.isDone()) {
                log.info("等茶叶...");
                TimeUnit.SECONDS.sleep(1);
            }

            String tea = t2Task.get();
            log.info("T1：拿到T2的{}，开始泡茶", tea);
            return "T1：上茶！！！";
        }
    }

    static class T2Task implements Callable<String> {
        @Override
        public String call() throws Exception {
            log.info("T2：洗茶壶...");
            TimeUnit.SECONDS.sleep(1);

            log.info("T2：洗茶杯...");
            TimeUnit.SECONDS.sleep(2);

            log.info("T2：拿茶叶...");
            TimeUnit.SECONDS.sleep(1);
            return "菊花茶";
        }
    }

}

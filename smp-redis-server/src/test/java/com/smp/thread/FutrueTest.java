package com.smp.thread;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * @author: fangxiaoyu
 * @date: 2021/7/1
 * @description:
 **/
@Slf4j
public class FutrueTest {

    private static ExecutorService executorService = new ThreadPoolExecutor(
            1,
            5,
            5,
            TimeUnit.SECONDS,
            new LinkedBlockingDeque<>());

    @Test
    public void test1() throws ExecutionException, InterruptedException, TimeoutException {
        Callable<String> callable = () -> {
            log.info("进入 Callable 的 call 方法");
            Thread.sleep(5000);
            return "Hello from Callable";
        };

        log.info("提交 Callable 到线程池");
        Future<String> future = executorService.submit(callable);

        log.info("主线程继续执行");
        log.info("主线程等待获取 future 结果");
        while (!future.isDone()) {
            log.info("异步还没结束");
            Thread.sleep(1000);
        }
        String result = future.get(8, TimeUnit.SECONDS);

        log.info("主线程获取到的 Future 结果：{}", result);
        executorService.shutdown();
    }
}

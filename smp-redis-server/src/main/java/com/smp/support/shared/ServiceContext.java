/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.support.shared;


import com.smp.domain.BaseInputDomain;
import com.smp.domain.BaseOutputDomain;
import com.smp.domain.ServiceMonitorModel;
import com.smp.enums.ServiceTypeEnum;

/**
 * 服务运行上下文：通用信息透传
 *
 * @author zhulang.jy
 * @version : ServiceContext.java, v 0.1 2020年10月14日 15:43 zhulang.jy Exp $
 */

public class ServiceContext<T extends BaseInputDomain, R extends BaseOutputDomain> {

	/**
	 * 服务类型，作为后续业务逻辑路由
	 */
	private ServiceTypeEnum serviceType = ServiceTypeEnum.UNKNOWN;

	/**
	 * 服务监控信息
	 */
	private ServiceMonitorModel monitorModel = new ServiceMonitorModel();

	/**
	 * 原始请求信息：仅用于日志打印，不可被随意消费
	 */
	private Object originInput;

	/**
	 * 业务请求幂等ID：能够唯一标示一次业务请求的ID
	 */
	private String requestUniqId;

	/**
	 * 服务请求
	 */
	T input;

	/**
	 * 服务结果
	 */
	R output;

	/**
	 * 服务上下文初始化
	 *
	 * @param serviceType
	 * @param originInput
	 */
	public void init(ServiceTypeEnum serviceType, Object originInput) {
		this.serviceType = serviceType;
		this.originInput = originInput;
		this.monitorModel.setServiceType(serviceType);
	}

	/**
	 * Getter method for property <tt>serviceType</tt>.
	 *
	 * @return property value of serviceType
	 */
	public ServiceTypeEnum getServiceType() {
		return serviceType;
	}

	/**
	 * Setter method for property <tt>serviceType</tt>.
	 *
	 * @param serviceType value to be assigned to property serviceType
	 */
	public void setServiceType(ServiceTypeEnum serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * Getter method for property <tt>monitorModel</tt>.
	 *
	 * @return property value of monitorModel
	 */
	public ServiceMonitorModel getMonitorModel() {
		return monitorModel;
	}

	/**
	 * Getter method for property <tt>originInput</tt>.
	 *
	 * @return property value of originInput
	 */
	public Object getOriginInput() {
		return originInput;
	}

	/**
	 * Getter method for property <tt>requestUniqId</tt>.
	 *
	 * @return property value of requestUniqId
	 */
	public String getRequestUniqId() {
		return requestUniqId;
	}

	/**
	 * Setter method for property <tt>requestUniqId</tt>.
	 *
	 * @param requestUniqId value to be assigned to property requestUniqId
	 */
	public void setRequestUniqId(String requestUniqId) {
		this.requestUniqId = requestUniqId;
		this.monitorModel.setRequestId(requestUniqId);
	}

	/**
	 * Getter method for property <tt>input</tt>.
	 *
	 * @return property value of input
	 */
	public T getInput() {
		return input;
	}

	/**
	 * Setter method for property <tt>input</tt>.
	 *
	 * @param input value to be assigned to property input
	 */
	public void setInput(T input) {
		this.input = input;
	}

	/**
	 * Getter method for property <tt>output</tt>.
	 *
	 * @return property value of output
	 */
	public R getOutput() {
		return output;
	}

	/**
	 * Setter method for property <tt>output</tt>.
	 *
	 * @param output value to be assigned to property output
	 */
	public void setOutput(R output) {
		this.output = output;
	}
}
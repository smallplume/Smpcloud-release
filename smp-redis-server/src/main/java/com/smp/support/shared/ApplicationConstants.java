package com.smp.support.shared;

/**
 * 应用常量类
 *
 * @author YangYi
 * @version v1.0
 * @date 2020 /05/08
 */
public class ApplicationConstants {

    private ApplicationConstants() {
    }

    /**
     * 有效记录
     */
    public static final Integer RECORD_IS_VALID = 0;
    /**
     * 无效记录
     */
    public static final Integer RECORD_IS_NOT_VALID = 1;

    /**
     * 已发布记录
     */
    public static final Integer RECORD_IS_PUBLISH = 1;

    /**
     * 未发布记录
     */
    public static final Integer RECORD_IS_NOT_PUBLISH = 0;

    /**
     * The constant MDC_TRACE_KEY.
     */
    public static final String MDC_TRACE_KEY = "traceId";

    /**
     * 默认字符集编码
     */
    private static final String DEFAULT_CHARSET = "UTF-8";

}

/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.support.shared;

/**
 * 日志名：后续需要做日志文件差异化治理，按类别、文件、以及日志生成策略等区分
 *
 * @author zhulang.jy
 * @version : LoggerNames.java, v 0.1 2020年10月14日 15:21 zhulang.jy Exp $
 */
public interface LoggerNames {

	/**
	 * 业务服务监控日志
	 */
	String EXPO_SERVICE_MONITOR = "EXPO_SERVICE_MONITOR";

	/**
	 * 业务服务详细日志
	 */
	String EXPO_SERVICE_DETAIL = "EXPO_SERVICE_DETAIL";

	String EXPO_INFRASTRUCTURE_DETAIL = "EXPO_INFRASTRUCTURE_DETAIL";
}
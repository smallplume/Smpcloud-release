package com.smp.support.restTemplate;

import lombok.Getter;
import org.springframework.http.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/15
 * @description:
 **/
@Getter
public class RestRequest {

    private final String url;

    private HttpMethod method;

    private Map<String, String> httpHeaders;

    private Class<?> classz;

    private Map<String, Object> params;

    public RestRequest(Builder builder) {
        this.url = builder.bUrl;
        this.method = builder.bMethod;
        this.httpHeaders = builder.bHttpHeaders;
        this.classz = builder.bClass;
        this.params = builder.bParams;
    }

    public static class Builder {
        private final String bUrl;

        private HttpMethod bMethod;

        private Map<String, String> bHttpHeaders = new HashMap<>();

        private Class<?> bClass;

        private Map<String, Object> bParams = new HashMap<>();

        public Builder(String url) {
            bUrl = url;
        }

        public Builder addMethod(HttpMethod method) {
            bMethod = method;
            return this;
        }

        public Builder addHttpHeader(String key, String value) {
            bHttpHeaders.put(key, value);
            return this;
        }

        public Builder addClassz(Class<?> classz) {
            bClass = classz;
            return this;
        }

        public Builder params(String key, Object value) {
            bParams.put(key, value);
            return this;
        }

        public RestRequest build() {
            this.bMethod = (this.bMethod == null ? HttpMethod.GET : this.bMethod);
            return new RestRequest(this);
        }
    }

}

package com.smp.support;

import com.smp.domain.DelayJobEntity;

/**
 * @author: xiaoyu
 * @date: 2020/4/23 ,9:19
 * @version: v1.0
 * @description:
 **/
public interface ExecuteJob {

    void execute(DelayJobEntity delayJob);
}

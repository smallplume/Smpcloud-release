package com.smp.support.execute;

import com.smp.domain.DelayJobEntity;
import com.smp.support.ExecuteJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * @author: xiaoyu
 * @date: 2020/4/23 ,9:27
 * @version: v1.0
 * @description:
 **/
@Slf4j
@Component
public class ExecuteJobImpl implements ExecuteJob {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");

    @Override
    public void execute(DelayJobEntity delayJob) {
        Date date = new Date();
        Iterator<Map.Entry<String, String>> iterator = delayJob.getJobParams().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String key = entry.getKey();
            String value = entry.getValue();
            log.info("{} 消息的结束时间 >>>> {}", value, sdf.format(date));
        }
    }

}

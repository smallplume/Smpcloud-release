package com.smp.dto;

import lombok.Data;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/17
 * @description:
 **/
@Data
public class RoleDTO {

    private Long id;

    private String name;

    private Integer age;
}

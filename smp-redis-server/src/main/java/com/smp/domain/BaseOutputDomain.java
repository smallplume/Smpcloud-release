/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.domain;


import com.smp.enums.ExpoErrorCodeEnum;

/**
 * 服务返回结果模型（应用内）
 * @author zhulang.jy
 * @version : BaseOutputDomain.java, v 0.1 2020年10月14日 16:05 zhulang.jy Exp $
 */
public class BaseOutputDomain extends BaseDomain {

    /**
     * 服务业务结果：服务请求结果明确失败
     */
    private boolean success = false;

    /**
     * 服务业务结果码（错误码）；
     */
    private ExpoErrorCodeEnum errorCode;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * Getter method for property <tt>success</tt>.
     *
     * @return property value of success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Setter method for property <tt>success</tt>.
     *
     * @param success  value to be assigned to property success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Getter method for property <tt>errorCode</tt>.
     *
     * @return property value of errorCode
     */
    public ExpoErrorCodeEnum getErrorCode() {
        return errorCode;
    }

    /**
     * Setter method for property <tt>errorCode</tt>.
     *
     * @param errorCode  value to be assigned to property errorCode
     */
    public void setErrorCode(ExpoErrorCodeEnum errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public BaseOutputDomain setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }

}
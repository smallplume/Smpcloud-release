package com.smp.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author: xiaoyu
 * @date: 2020/4/23 ,9:20
 * @version: v1.0
 * @description:
 **/
@Data
public class DelayJobEntity implements Serializable {

    /**
     * job执行参数
     */
    private Map jobParams;

    /**
     * 具体执行实例实现
     */
    private Class aClass;

}

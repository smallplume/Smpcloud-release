/**
 * Dream Team Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.domain;

import lombok.Data;

/**
 * 查询单实例返回结果
 *
 * @author zhulang.jy
 * @version : SmpQueryResult.java, v 0.1 2020年11月17日 23:41 zhulang.jy Exp $
 */
@Data
public class SmpQueryResult<T> extends SmpBaseResult {

	private static final long serialVersionUID = 6558791424483783674L;
	/**
	 * 查询的返回结果
	 */
	private T resultInfo;

}
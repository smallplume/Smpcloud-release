/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.domain;

import com.smp.enums.IsDeleteEnum;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 领域模型基类：后续领域模型的公共属性归属在此
 * @author zhulang.jy
 * @version : BaseDomain.java, v 0.1 2020年10月14日 15:55 zhulang.jy Exp $
 */
public class BaseDomain implements Serializable {

    private static final long serialVersionUID = 5746843721484496166L;

    /**
     * 数据版本:默认初始为1
     */
    private Integer version = 1;

    /**
     * 创建时间(注意这里防错，默认以DB设置为准）
     */
    private Date createTime = new Date();

    /**
     * 修改时间(注意这里防错，默认以DB设置为准）
     */
    private Date modifyTime = new Date();

    /**删除标示（注意这里防错，默认以DB中实际值为准*/
    private IsDeleteEnum deleteFlag = IsDeleteEnum.NOT_DELETED;

    /**
     * 扩展信息：某些模型可能不需要
     */
    private Map<String, Object> extendInfo = new HashMap<>();

    /**
     * Getter method for property <tt>version</tt>.
     *
     * @return property value of version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter method for property <tt>version</tt>.
     *
     * @param version  value to be assigned to property version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter method for property <tt>createTime</tt>.
     *
     * @return property value of createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * Setter method for property <tt>createTime</tt>.
     *
     * @param createTime  value to be assigned to property createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * Getter method for property <tt>modifyTime</tt>.
     *
     * @return property value of modifyTime
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * Setter method for property <tt>modifyTime</tt>.
     *
     * @param modifyTime  value to be assigned to property modifyTime
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * Getter method for property <tt>extendInfo</tt>.
     *
     * @return property value of extendInfo
     */
    public Map<String, Object> getExtendInfo() {
        return extendInfo;
    }

    /**
     * Setter method for property <tt>extendInfo</tt>.
     *
     * @param extendInfo  value to be assigned to property extendInfo
     */
    public void setExtendInfo(Map<String, Object> extendInfo) {
        this.extendInfo.putAll(extendInfo);
    }

    /**
     * Getter method for property <tt>deleteFlag</tt>.
     *
     * @return property value of deleteFlag
     */
    public IsDeleteEnum getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * Setter method for property <tt>deleteFlag</tt>.
     *
     * @param deleteFlag  value to be assigned to property deleteFlag
     */
    public void setDeleteFlag(IsDeleteEnum deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
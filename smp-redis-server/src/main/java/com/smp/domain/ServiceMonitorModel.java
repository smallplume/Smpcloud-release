/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.domain;

import com.smp.enums.ExpoErrorCodeEnum;
import com.smp.enums.ServiceTypeEnum;
import com.smp.util.DateUtils;
import lombok.Data;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author zhulang.jy
 * @version : ServiceMonitorModel.java, v 0.1 2020年10月14日 15:47 zhulang.jy Exp $
 */
@Data
public class ServiceMonitorModel {

    /**服务类型，作为后续业务逻辑路由*/
    private ServiceTypeEnum serviceType = ServiceTypeEnum.UNKNOWN;

    /**业务的请求ID（能够唯一识别一次请求的ID，需要根据具体服务场景构造）*/
    private String requestId;

    /**服务运行结果*/
    private ExpoErrorCodeEnum resultCode;

    /**业务结果*/
    private Result result = Result.FAILED;

    /**服务开始时间*/
    private long startTime;

    /**服务结束时间*/
    private long elapseTime;

    ///////////////////////业务相关的字段////////////////////////////////

    /**请求业务参数信息:简写，用于监控日志*/
    private String requestShortBizInfo;

    /**
     * 业务服务执行结果，用于打点日志模型
     * @author zhulang.jy
     * @version $Id: ServiceMonitorModel.java, v 0.1 2016年4月13日 下午8:53:03 zhulang.jy Exp $
     */
    public enum Result {
        /** 执行成功 */
        SUCCESS("S"),

        /**执行失败  */
        FAILED("F");

        /**打点日志中输出的值  */
        private String showFlag;

        /**
         * 初始化
         * @param showFlag  打点日志中输出的值
         */
        Result(String showFlag) {
            this.showFlag = showFlag;
        }

        /**
         * Getter method for property <tt>showFlag</tt>.
         *
         * @return property value of showFlag
         */
        public String getShowFlag() {
            return showFlag;
        }

    }

    /**
     * 返回待输出的格式化监控日志
     * @return 待打印日志内容
     */
    public String showLog() {

        //基本信息
        String baseMonitor = MessageFormat
                .format(
                        "[ServiceType:{0}; RequestId:{1}; Result:{2}; ResultCode:{3}; StartTime:{4}; StartTimeFormat:{5}; ElapseTime:{6}]",
                        serviceType, requestId,
                        result.getShowFlag(), resultCode == null ? "SUCCESS" : resultCode.getErrorCode(), String.valueOf(startTime),
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(startTime), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(
                                DateUtils.DEFAULT_DATETIME_PATTERN)), String.valueOf(elapseTime));

        //业务信息
        String bizMonitor = MessageFormat.format(
                "[ RequestBizInfo:{0}]", requestShortBizInfo);

        return baseMonitor + bizMonitor;
    }

    /**
     * 计算流失时间
     */
    public void recordElapseTime() {
        this.elapseTime = System.currentTimeMillis() - startTime;
    }

    /**
     * 追加业务请求简短信息（监控用）
     * @param requestBizInfo
     * @return
     */
    public ServiceMonitorModel appendRequestBizInfo(String requestBizInfo) {

        if (this.requestShortBizInfo == null) {
            this.requestShortBizInfo = new StringBuilder().append(requestBizInfo).toString();
        } else {
            this.requestShortBizInfo = new StringBuilder(this.requestShortBizInfo).append("|")
                    .append(requestBizInfo).toString();
        }
        return this;
    }
}
/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 标准服务的返回结果对象基类；注意，复杂的返回对象一般应集成
 *
 * @author zhulang.jy
 * @version : ExpoBaseResult.java, v 0.1 2020年10月14日 17:17 zhulang.jy Exp $
 */

@Data
public class SmpBaseResult implements Serializable {

	private static final long serialVersionUID = 5130734612955098720L;

	private String traceId;

	private boolean success;

	private String resultCode;

	private String errorMsg;

	/**
	 * 扩展信息
	 */
	private Map<String,Object> extInfo;
}
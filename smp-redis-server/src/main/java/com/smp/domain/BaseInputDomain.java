/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.domain;

/**
 * 服务请求模型（应用内）
 *
 * @author zhulang.jy
 * @version : BaseInputDomain.java, v 0.1 2020年10月14日 16:05 zhulang.jy Exp $
 */
public class BaseInputDomain extends BaseDomain {


}
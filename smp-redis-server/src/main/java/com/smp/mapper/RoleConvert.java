package com.smp.mapper;

import com.smp.dto.RoleDTO;
import com.smp.vo.RoleDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/17
 * @description:
 **/
@Mapper(componentModel = "spring")
public interface RoleConvert {

    RoleDO convert(RoleDTO roleDTO);

    @Mappings(@Mapping(source = "age1", target = "age"))
    RoleDTO convert(RoleDO roleDO);

}

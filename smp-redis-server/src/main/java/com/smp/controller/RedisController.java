package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/10
 * @description:
 **/
@Slf4j
@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("/get")
    public BaseVo getRedisValue(@RequestParam(value = "val", required = false) String val) {
        // redisTemplate.execute();
        return new BaseVo();
    }

    @GetMapping("/test")
    public void decrByUntil0Lua() {
        // 存货10个
        redisTemplate.opsForValue().set("goodsId", 10);
        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(5);

        String script = " local leftvalue = redis.call('get', KEYS[1]); "
                + " if ARGV[1] - leftvalue > 0 then return nil; else "
                + " return redis.call('decrby', KEYS[1], ARGV[1]); end; ";
        for (int i = 0; i < 20; i++) {
            int finalI = i;
            fixedThreadPool.execute(() -> {
                DefaultRedisScript<Long> rs = new DefaultRedisScript<>();
                //设置脚本
                rs.setScriptText(script);
                //定义返回类型。注意如果没有这个定义，spring不会返回结果
                rs.setResultType(Long.class);
                RedisSerializer valueSerializer = redisTemplate.getValueSerializer();
                RedisSerializer stringSerializer = redisTemplate.getStringSerializer();
                List<String> keyList = new ArrayList<>();
                keyList.add("goodsId");
                List<Integer> valsList = new ArrayList<>();
                // 减去的个数
                valsList.add(1);
                Long restult = (Long) redisTemplate.execute(rs, valueSerializer, stringSerializer, keyList, valsList.toArray());
                if (null != restult) {
                    log.info("{}，i：{} 剩下商品数量：{}",Thread.currentThread().getName(), finalI, restult);
                } else {
                    log.info("没货了");
                }
            });
        }
    }

}

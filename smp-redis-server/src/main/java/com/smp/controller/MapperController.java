package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.dto.RoleDTO;
import com.smp.mapper.RoleConvert;
import com.smp.vo.RoleDO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/17
 * @description:
 **/
@RestController
@RequestMapping("/mapper")
public class MapperController {

    @Resource
    private RoleConvert roleConvert;

    @GetMapping("/convert")
    public BaseVo convert() {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(10086L);
        roleDTO.setName("中国移动");
        RoleDO roleDO = roleConvert.convert(roleDTO);
        return new BaseVo(roleDO);
    }

    @GetMapping("/convert2")
    public BaseVo convert2() {
        RoleDO roleDO = new RoleDO();
        roleDO.setId(10087L);
        roleDO.setName("中国移动2");
        roleDO.setAge1(11);
        RoleDTO roleDTO = roleConvert.convert(roleDO);
        return new BaseVo(roleDTO);
    }

}

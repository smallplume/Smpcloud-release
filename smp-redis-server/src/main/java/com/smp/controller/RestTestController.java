package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.support.restTemplate.RestClient;
import com.smp.support.restTemplate.RestRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/15
 * @description:
 **/
@RestController
@RequestMapping("/rest")
public class RestTestController {

    @Resource
    private RestClient restClient;

    @GetMapping("/test")
    private BaseVo getResult() {
        ResponseEntity responseEntity = restClient.execute(new RestRequest.Builder("http://127.0.0.1:8091/rest/test1")
                .addMethod(HttpMethod.GET)
                .addClassz(String.class)
                .build());
        return new BaseVo(responseEntity);
    }

}

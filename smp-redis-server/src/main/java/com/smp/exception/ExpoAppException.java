/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.exception;

import com.smp.enums.ExpoErrorCodeEnum;
import org.apache.commons.lang3.StringUtils;

/**
 * 本应用异常
 *
 * @author zhulang.jy
 * @version : ExpoAppException.java, v 0.1 2020年10月13日 22:09 zhulang.jy Exp $
 */
public class ExpoAppException extends RuntimeException {

	/**
	 * 错误码枚举
	 */
	private ExpoErrorCodeEnum errorCodeEnum = ExpoErrorCodeEnum.SYSTEM_EXCEPTION;

	/**
	 * 默认构造器
	 */
	public ExpoAppException() {
		super();
	}

	/**
	 * 构造器
	 *
	 * @param msg 错误描述
	 */
	public ExpoAppException(String msg) {
		super(msg);
	}

	/**
	 * 构造器
	 *
	 * @param cause 错误对象
	 */
	public ExpoAppException(Throwable cause) {
		super(cause);
	}

	/**
	 * 构造器
	 *
	 * @param msg   错误信息
	 * @param cause 错误对象
	 */
	public ExpoAppException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * The construct method
	 *
	 * @param errorCodeEnum 错误码
	 */
	public ExpoAppException(ExpoErrorCodeEnum errorCodeEnum) {
		super(errorCodeEnum.getDescription());
		this.errorCodeEnum = errorCodeEnum;
	}

	/**
	 * The construct method
	 *
	 * @param errorCodeEnum 错误码
	 * @param msg           错误描述
	 */
	public ExpoAppException(ExpoErrorCodeEnum errorCodeEnum, String msg) {
		super(msg);
		this.errorCodeEnum = errorCodeEnum;
	}

	/**
	 * 异常
	 *
	 * @param t             异常
	 * @param errorCodeEnum 错误码
	 * @param msg           内容
	 */
	public ExpoAppException(Throwable t, ExpoErrorCodeEnum errorCodeEnum, String msg) {
		super(msg, t);
		this.errorCodeEnum = errorCodeEnum;
	}

	/**
	 * The construct method
	 *
	 * @param errorCodeEnum 错误码
	 * @param t             错误对象
	 */
	public ExpoAppException(ExpoErrorCodeEnum errorCodeEnum, Throwable t) {
		super(t);
		this.errorCodeEnum = errorCodeEnum;
	}

	/**
	 * Getter method for property errorCodeEnum.
	 *
	 * @return property value of errorCodeEnum
	 */
	public ExpoErrorCodeEnum getErrorCodeEnum() {
		return errorCodeEnum;
	}

	/**
	 * Setter method for property errorCodeEnum.
	 *
	 * @param errorCodeEnum value to be assigned to property errorCodeEnum
	 */
	public void setErrorCodeEnum(ExpoErrorCodeEnum errorCodeEnum) {
		this.errorCodeEnum = errorCodeEnum;
	}

	/**
	 * @see Object#toString()
	 */
	@Override
	public String toString() {
		String s = getClass().getName();
		String message = getLocalizedMessage();
		return (StringUtils.isNotBlank(message)) ? (s + ": " + message)
				: (s + ": " + errorCodeEnum.getErrorCode() + "[" + errorCodeEnum.getDescription() + "].");
	}
}
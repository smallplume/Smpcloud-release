package com.smp.service;

import com.smp.domain.DelayJobEntity;
import com.smp.support.JobTimer;
import lombok.Data;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * @author: xiaoyu
 * @date: 2020/4/23 ,9:22
 * @version: v1.0
 * @description:
 **/
@Data
@Component
public class DelayJobService implements Serializable {

    @Autowired
    private RedissonClient client;

    public void submitJob(DelayJobEntity job, Long delay, TimeUnit timeUnit) {
        RBlockingQueue blockingQueue = client.getBlockingQueue(JobTimer.jobsTag);
        RDelayedQueue delayedQueue = client.getDelayedQueue(blockingQueue);
        delayedQueue.offer(job, delay, timeUnit);
    }
}

package com.smp.vo;

import lombok.Data;

/**
 * @author: fangxiaoyu
 * @date: 2021/6/17
 * @description:
 **/
@Data
public class RoleDO {

    private Long id;

    private String name;

    private Integer age1;

}

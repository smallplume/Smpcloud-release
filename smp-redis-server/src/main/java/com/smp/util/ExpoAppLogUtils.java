/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.smp.util;

import org.slf4j.Logger;

import java.text.MessageFormat;

/**
 * 支持格式化日志工具
 * @author zhulang.jy
 * @version : ExpoAppLogUtil.java, v 0.1 2020年10月13日 17:33 zhulang.jy Exp $
 */
public class ExpoAppLogUtils {

    private ExpoAppLogUtils() {
    }

    /**
     * <p>生成通知级别日志</p>
     * <p>
     * 根据带参数的日志模板和参数集合，生成info级别日志
     * 带参数的日志模板中以{数字}表示待替换为变量的日志点，如a={0}，表示用参数集合中index为0的参数替换{0}处
     * </p>
     * @param logger        logger引用
     * @param template      带参数的日志模板
     * @param parameters    参数集合
     */
    public static void info(Logger logger, String template, Object... parameters) {

        if (logger.isInfoEnabled()) {
            String log = MessageFormat.format(template, parameters);
            logger.info(log);
        }

    }

    /**
     * 打印warn日志
     * @param logger
     * @param template
     * @param parameters
     */
    public static void warn(Logger logger, String template, Object... parameters) {

        if (logger.isWarnEnabled()) {
            String log = MessageFormat.format(template, parameters);
            logger.warn(log);
        }

    }

    /**
     * 打印warn日志
     * @param e
     * @param logger
     * @param template
     * @param parameters
     */
    public static void warn(Throwable e, Logger logger, String template, Object... parameters) {

        if (logger.isWarnEnabled()) {
            String log = MessageFormat.format(template, parameters);
            logger.warn(log, e);
        }

    }

    /**
     * 打印debug日志
     * @param logger
     * @param template
     * @param parameters
     */
    public static void debug(Logger logger, String template, Object... parameters) {

        if (logger.isDebugEnabled()) {
            String log = MessageFormat.format(template, parameters);
            logger.debug(log);
        }

    }

    /**
     * 打印error日志
     * @param logger
     * @param template
     * @param parameters
     */
    public static void error(Logger logger, String template, Object... parameters) {

        if (logger.isErrorEnabled()) {
            String log = MessageFormat.format(template, parameters);
            logger.error(log);
        }

    }

    /**
     * 打印error日志
     * @param e
     * @param logger
     * @param template
     * @param parameters
     */
    public static void error(Throwable e, Logger logger, String template, Object... parameters) {

        if (logger.isErrorEnabled()) {
            String log = MessageFormat.format(template, parameters);
            logger.error(log, e);
        }

    }
}
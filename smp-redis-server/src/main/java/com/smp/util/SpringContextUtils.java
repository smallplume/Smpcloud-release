package com.smp.util;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * The type Spring context util.
 *
 * @author Daniel
 * @date 2020 -05-14 14:33
 */
@Component
@Slf4j
public class SpringContextUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    /**
     * 实现ApplicationContextAware接口的回调方法，设置上下文环境
     *
     * @param applicationContext
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        SpringContextUtils.applicationContext = applicationContext;
    }

    /**
     * Gets application context.
     *
     * @return ApplicationContext application context
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 获取对象
     * 这里重写了bean方法，起主要作用
     *
     * @param name the name
     * @return Object 一个以所给名字注册的bean的实例
     * @throws BeansException the beans exception
     */
    public static Object getBean(String name) throws BeansException {
        return applicationContext.getBean(name);
    }

    /**
     * Gets property.
     *
     * @param key the key
     * @return the property
     */
    public static String getProperty(String key) {
        return applicationContext.getEnvironment().getProperty(key);
    }

    /**
     * Gets environment.
     *
     * @return the environment
     */
    public static Environment getEnvironment() {
        return applicationContext.getEnvironment();
    }

    /**
     * Gets bean.
     *
     * @param <T>   the type parameter
     * @param clazz the clazz
     * @return the bean
     */
    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    /**
     * Is produce environment boolean.
     *
     * @return the boolean
     */
    public static boolean isProduceEnvironment() {
        List<String> activeProfiles = Lists.newArrayList(applicationContext.getEnvironment().getActiveProfiles());
        if (CollectionUtils.isEmpty(activeProfiles)) {
            return false;
        }
        return activeProfiles.contains("prod");
    }

    /**
     * Is test environment boolean.
     *
     * @return the boolean
     */
    public static boolean isTestEnvironment() {
        List<String> activeProfiles = Lists.newArrayList(applicationContext.getEnvironment().getActiveProfiles());
        if (CollectionUtils.isEmpty(activeProfiles)) {
            return false;
        }
        return activeProfiles.contains("test");
    }

    /**
     * Is specific environment boolean.
     *
     * @param envStr the env str
     * @return the boolean
     */
    public static boolean isSpecificEnvironment(String envStr) {
        List<String> activeProfiles = Lists.newArrayList(applicationContext.getEnvironment().getActiveProfiles());
        if (CollectionUtils.isEmpty(activeProfiles)) {
            return false;
        }
        return activeProfiles.contains(envStr);
    }


}

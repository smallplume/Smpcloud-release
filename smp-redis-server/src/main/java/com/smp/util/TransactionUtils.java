package com.smp.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;

/**
 * <p>
 *
 * </p>
 *
 * @author Guo Liang
 * @version v1.0
 * @date 2021/1/26 9:34
 */
@Component
public class TransactionUtils {
    public static final String METHOD_PROCESS_NAME = "simpleProcess";

    @Autowired
    private DataSourceTransactionManager dataSourceTransactionManager;

    /**
     * 初始化创建TransactionStatus对象
     *
     * @return
     */
    public TransactionStatus init() {
        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(new DefaultTransactionAttribute());
        return transactionStatus;
    }

    /**
     * 提交事务
     *
     * @param transactionStatus
     */
    public void commit(TransactionStatus transactionStatus) {
        dataSourceTransactionManager.commit(transactionStatus);
    }


    public void rollback(TransactionStatus transactionStatus) {
        dataSourceTransactionManager.rollback(transactionStatus);
    }
}

/**
 * Dream Team Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package com.smp.enums;

/**
 * TODO
 * @author zhulang.jy
 * @version : IsDeleteEnum.java, v 0.1 2020年11月25日 00:49 zhulang.jy Exp $
 */
public enum IsDeleteEnum {
    /**
     * 已删除
     */
    DELETED(1, "已删除"),
    /**未删除*/
    NOT_DELETED(0, "未删除"),

    ;

    /**删除标示，对应在DB中持久化的值*/
    private Integer flag;

    /**描述*/
    private String desc;

    /**
     * 构造函数
     * @param flag
     * @param desc
     */
    IsDeleteEnum(int flag, String desc) {

        this.flag = flag;
        this.desc = desc;
    }

    /**
     * 根据枚举码获取枚举
     *
     * @param flag      枚举码
     * @return 枚举
     */
    public static IsDeleteEnum getByFlag(Integer flag) {

        for (IsDeleteEnum tmpStatus : IsDeleteEnum.values()) {
            if (tmpStatus.getFlag().equals(flag)) {
                return tmpStatus;
            }
        }

        return NOT_DELETED;
    }

    /**
     * Getter method for property <tt>flag</tt>.
     *
     * @return property value of flag
     */
    public Integer getFlag() {
        return flag;
    }

    /**
     * Getter method for property <tt>desc</tt>.
     *
     * @return property value of desc
     */
    public String getDesc() {
        return desc;
    }
}
package com.smp.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 应用服务类型（后续持续管控和治理）
 *
 * @author zhulang.jy
 * @version : ServiceType.java, v 0.1 2020年10月14日 15:29 zhulang.jy Exp $
 */
public enum ServiceTypeEnum {

    /**
     * 我的询盘
     */
    MY_INQUIRY("MY_INQUIRY", "my inquiry list"),

    /**
     * 其它
     */
    UNKNOWN("UNKNOWN", "unknown");

    /**
     * code
     */
    private final String code;

    /**
     * desc
     */
    private final String desc;

    /**
     * Default construct method
     *
     * @param code code
     * @param desc desc
     */
    ServiceTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * Get the enum object by code
     *
     * @param code code
     * @return IpTypeEnum
     */
    public static ServiceTypeEnum getByCode(String code) {
        if (StringUtils.isBlank(code)) {
            return UNKNOWN;
        }

        for (ServiceTypeEnum tmpType : ServiceTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(code, tmpType.getCode())) {
                return tmpType;
            }
        }

        return UNKNOWN;
    }

    /**
     * Getter method for property <tt>code</tt>.
     *
     * @return property value of code
     */
    public String getCode() {
        return code;
    }

    /**
     * Getter method for property <tt>desc</tt>.
     *
     * @return property value of desc
     */
    public String getDesc() {
        return desc;
    }
}

/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : nacos

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 07/03/2020 16:44:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_use` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `effect` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_schema` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (6, 'nacos-server.properties', 'DEFAULT_GROUP', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=this is public enviroment\r\nnacos=hahaha', 'cc76651babd14ad0e8b8e753546a2a1c', '2020-03-07 00:08:08', '2020-03-07 16:38:20', NULL, '127.0.0.1', '', '', 'null', NULL, NULL, 'properties', NULL);
INSERT INTO `config_info` VALUES (11, 'nacos-server.properties', 'DEFAULT_GROUP', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=this is dev enviroment', '383ab5f7741b8c7338eb68c37ff17ce5', '2020-03-07 13:06:55', '2020-03-07 13:10:16', NULL, '127.0.0.1', '', '8f1122fe-cd92-4680-9a37-f346cdf67514', '开发环境', NULL, NULL, 'properties', NULL);
INSERT INTO `config_info` VALUES (14, 'nacos-server.properties', 'DEFAULT_GROUP', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=this is product enviroment', '8f41e268ab91ee09eb8ec38936f29aab', '2020-03-07 13:16:04', '2020-03-07 13:16:32', NULL, '127.0.0.1', '', '9e47795f-e004-4768-b8e9-4ea0635b4223', '生产环境', NULL, NULL, 'properties', NULL);
INSERT INTO `config_info` VALUES (16, 'nacos-server.properties', 'DEFAULT_GROUP', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=this is test enviroment', '64c32c635c6413374e2d1227b3924833', '2020-03-07 13:18:27', '2020-03-07 13:18:53', NULL, '127.0.0.1', '', '10df807b-ed26-4ce3-b0f8-24c9fc22ed83', '', NULL, NULL, 'properties', NULL);

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime(0) NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfoaggr_datagrouptenantdatum`(`data_id`, `group_id`, `tenant_id`, `datum_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '增加租户字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_beta' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id`, `group_id`, `tenant_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_tag' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id`, `tag_name`, `tag_type`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_tag_relation' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint(64) UNSIGNED NOT NULL,
  `nid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `op_type` char(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create`) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified`) USING BTREE,
  INDEX `idx_did`(`data_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多租户改造' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------
INSERT INTO `his_config_info` VALUES (0, 1, 'nacos-server-01', 'DEFAULT_GROUP', '', 'nacosTest: 10086', '6199f25f2da976d5f0ea390297018d86', '2010-05-05 00:00:00', '2020-03-06 22:59:21', NULL, '127.0.0.1', 'I', '');
INSERT INTO `his_config_info` VALUES (1, 2, 'nacos-server-01', 'DEFAULT_GROUP', '', 'nacosTest: 10086', '6199f25f2da976d5f0ea390297018d86', '2010-05-05 00:00:00', '2020-03-06 23:04:25', NULL, '127.0.0.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 3, 'nacos-server-01.yaml', 'DEFAULT_GROUP', '', 'nacosTest: yaml格式的', '39ef1b4e9906341d1290278188abc91f', '2010-05-05 00:00:00', '2020-03-06 23:07:16', NULL, '127.0.0.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 4, 'nacos-server-01.properties', 'DEFAULT_GROUP', '', 'nacosTest=properties的配置文件', 'ee187648739c4977cf5bfb905612892d', '2010-05-05 00:00:00', '2020-03-06 23:09:18', NULL, '127.0.0.1', 'I', '');
INSERT INTO `his_config_info` VALUES (3, 5, 'nacos-server-01.yaml', 'DEFAULT_GROUP', '', 'nacosTest: yaml格式的', '39ef1b4e9906341d1290278188abc91f', '2010-05-05 00:00:00', '2020-03-06 23:35:02', NULL, '127.0.0.1', 'D', '');
INSERT INTO `his_config_info` VALUES (1, 6, 'nacos-server-01', 'DEFAULT_GROUP', '', 'nacosTest: jdbc://mysql:3306/test', '88dd699fe1a04e3e0bb0f98e8915f717', '2010-05-05 00:00:00', '2020-03-06 23:35:06', NULL, '127.0.0.1', 'D', '');
INSERT INTO `his_config_info` VALUES (4, 7, 'nacos-server-01.properties', 'DEFAULT_GROUP', '', 'nacosTest=properties的配置文件', 'ee187648739c4977cf5bfb905612892d', '2010-05-05 00:00:00', '2020-03-06 23:35:49', NULL, '127.0.0.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 8, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=\r\nmysql.use=', 'af65c33a71ca7ccfdad73d740a636ecd', '2010-05-05 00:00:00', '2020-03-07 00:08:08', NULL, '127.0.0.1', 'I', '');
INSERT INTO `his_config_info` VALUES (6, 9, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=\r\nmysql.use=', 'af65c33a71ca7ccfdad73d740a636ecd', '2010-05-05 00:00:00', '2020-03-07 00:08:24', NULL, '127.0.0.1', 'U', '');
INSERT INTO `his_config_info` VALUES (4, 10, 'nacos-server-01.properties', 'DEFAULT_GROUP', '', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=properties的配置文件', '31acc01550749035f95bc4181f29858a', '2010-05-05 00:00:00', '2020-03-07 00:10:36', NULL, '127.0.0.1', 'D', '');
INSERT INTO `his_config_info` VALUES (6, 11, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=properties的配置文件', '31acc01550749035f95bc4181f29858a', '2010-05-05 00:00:00', '2020-03-07 13:04:48', NULL, '127.0.0.1', 'U', '');
INSERT INTO `his_config_info` VALUES (6, 12, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=这是public默认的环境的数据', '84b86270391afb2e275ffa59de0ec188', '2010-05-05 00:00:00', '2020-03-07 13:05:01', NULL, '127.0.0.1', 'U', '');
INSERT INTO `his_config_info` VALUES (6, 13, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=这是public默认的环境的数据', '84b86270391afb2e275ffa59de0ec188', '2010-05-05 00:00:00', '2020-03-07 13:05:27', NULL, '127.0.0.1', 'U', '');
INSERT INTO `his_config_info` VALUES (0, 14, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=\r\nmysql.username=\r\nmysql.password=\r\nnacosTest=this is dev enviroment', '6d876f884d093b264d196540ebf4d29e', '2010-05-05 00:00:00', '2020-03-07 13:06:55', NULL, '127.0.0.1', 'I', '8f1122fe-cd92-4680-9a37-f346cdf67514');
INSERT INTO `his_config_info` VALUES (11, 15, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=\r\nmysql.username=\r\nmysql.password=\r\nnacosTest=this is dev enviroment', '6d876f884d093b264d196540ebf4d29e', '2010-05-05 00:00:00', '2020-03-07 13:07:20', NULL, '127.0.0.1', 'U', '8f1122fe-cd92-4680-9a37-f346cdf67514');
INSERT INTO `his_config_info` VALUES (11, 16, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=this is dev enviroment', '383ab5f7741b8c7338eb68c37ff17ce5', '2010-05-05 00:00:00', '2020-03-07 13:10:16', NULL, '127.0.0.1', 'U', '8f1122fe-cd92-4680-9a37-f346cdf67514');
INSERT INTO `his_config_info` VALUES (0, 17, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=', '3ed276c0a02595aef4203e04d5bc111e', '2010-05-05 00:00:00', '2020-03-07 13:16:04', NULL, '127.0.0.1', 'I', '9e47795f-e004-4768-b8e9-4ea0635b4223');
INSERT INTO `his_config_info` VALUES (14, 18, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=', '3ed276c0a02595aef4203e04d5bc111e', '2010-05-05 00:00:00', '2020-03-07 13:16:32', NULL, '127.0.0.1', 'U', '9e47795f-e004-4768-b8e9-4ea0635b4223');
INSERT INTO `his_config_info` VALUES (0, 19, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=', '3ed276c0a02595aef4203e04d5bc111e', '2010-05-05 00:00:00', '2020-03-07 13:18:27', NULL, '127.0.0.1', 'I', '10df807b-ed26-4ce3-b0f8-24c9fc22ed83');
INSERT INTO `his_config_info` VALUES (16, 20, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=', '3ed276c0a02595aef4203e04d5bc111e', '2010-05-05 00:00:00', '2020-03-07 13:18:53', NULL, '127.0.0.1', 'U', '10df807b-ed26-4ce3-b0f8-24c9fc22ed83');
INSERT INTO `his_config_info` VALUES (6, 21, 'nacos-server.properties', 'DEFAULT_GROUP', '', 'mysql.url=jdbc:mysql://localhost:3306/smpcloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC\r\nmysql.username=root\r\nmysql.password=root\r\nnacosTest=this is public enviroment', '8406eef84527cd6f2ef7498292bdbe6c', '2010-05-05 00:00:00', '2020-03-07 16:38:20', NULL, '127.0.0.1', 'U', '');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '租户容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp`, `tenant_id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'tenant_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_info
-- ----------------------------
INSERT INTO `tenant_info` VALUES (1, '1', '8f1122fe-cd92-4680-9a37-f346cdf67514', 'dev', '开发环境', 'nacos', 1583557337401, 1583557355965);
INSERT INTO `tenant_info` VALUES (2, '1', '9e47795f-e004-4768-b8e9-4ea0635b4223', 'product', '生产环境', 'nacos', 1583558103443, 1583558103443);
INSERT INTO `tenant_info` VALUES (3, '1', '10df807b-ed26-4ce3-b0f8-24c9fc22ed83', 'test', '测试环境', 'nacos', 1583558208741, 1583558208741);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', 1);

SET FOREIGN_KEY_CHECKS = 1;

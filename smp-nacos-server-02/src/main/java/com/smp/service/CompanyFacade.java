package com.smp.service;

import com.smp.dto.CompanyDTO;

/**
 * @author SmallPlume
 * @title: CompanyFacade
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 23:55
 */
public interface CompanyFacade {

    void inc(CompanyDTO companyDTO);
}

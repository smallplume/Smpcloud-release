package com.smp.service.impl;

import com.smp.dao.CompanyMapper;
import com.smp.dto.CompanyDTO;
import com.smp.entity.Company;
import com.smp.service.CompanyFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author SmallPlume
 * @title: CompanyFacadeImpl
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 23:56
 */
@Slf4j
@Service
public class CompanyFacadeImpl implements CompanyFacade {

    @Autowired
    private CompanyMapper companyMapper;

    @Override
    public void inc(CompanyDTO companyDTO) {
        Company company = Company.DTOtransitionEntity(companyDTO);
        companyMapper.insert(company);
    }
}

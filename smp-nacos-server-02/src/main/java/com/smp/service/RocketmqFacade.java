package com.smp.service;

/**
 * @author SmallPlume
 * @title: RocketmqFacade
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/16 15:53
 */
public interface RocketmqFacade {

    void sendMes(String msg);
}

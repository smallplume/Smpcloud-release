package com.smp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(value = "com.smp.dao")
public class SmpNacosServer02Application {

    public static void main(String[] args) {
        SpringApplication.run(SmpNacosServer02Application.class, args);
    }

}

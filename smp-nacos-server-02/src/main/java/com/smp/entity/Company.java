package com.smp.entity;

import com.smp.dto.CompanyDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author SmallPlume
 * @title: Company
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 23:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Company implements Serializable {

    private Integer id;

    private String name;

    private Integer legalPerson;

    public static Company DTOtransitionEntity(CompanyDTO companyDTO) {
        Company company = new Company();
        company.setName(companyDTO.getName());
        company.setLegalPerson(companyDTO.getLegalPerson());
        return company;
    }

}
package com.smp.controller;

import com.smp.dto.CompanyDTO;
import com.smp.service.CompanyFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author SmallPlume
 * @title: CompanyController
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/7 0:00
 */
@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyFacade companyFacade;

    @PostMapping("/insert")
    public void insert(@RequestBody CompanyDTO companyDTO) {
        companyFacade.inc(companyDTO);
    }
}

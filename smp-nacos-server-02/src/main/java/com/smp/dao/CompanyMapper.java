package com.smp.dao;

import com.smp.dto.CompanyDTO;
import com.smp.entity.Company;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author SmallPlume
 * @title: CompanyMapper
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/6 23:52
 */
public interface CompanyMapper {

    void insert(Company company);

    @Select("Select id, name, legal_person from company where id = #{id}")
    CompanyDTO queryCompanyById(@Param(value = "id") Integer id);
}

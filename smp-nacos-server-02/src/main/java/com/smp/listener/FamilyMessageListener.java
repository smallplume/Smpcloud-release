package com.smp.listener;

import com.smp.config.JmsConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

/**
 * @author SmallPlume
 * @title: FamilyMessageListener
 * @projectName Smpcloud-release
 * @description: 测试重试机制
 * @date 2020/3/17 0:27
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = JmsConfig.TOPIC, selectorExpression = "testtag1", consumerGroup = "test_consumer", messageModel = MessageModel.CLUSTERING /* 消息类型：集群、广播*/)
public class FamilyMessageListener implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt message) {
        try {
            String body = new String(message.getBody(), "utf-8");
            log.info("Consumer-获取消息-主题topic为={}, 消费消息为={}, 时间 ========================={}", message.getTopic(), body, System.currentTimeMillis());
            // 使程序报错，触发重试机制
            int i = 10 / 0;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}

package com.smp.listener;

import com.smp.config.JmsConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

/**
 * @author SmallPlume
 * @title: FamilyMessageListener
 * @projectName Smpcloud-release
 * @description: 监听器
 * @date 2020/3/16 13:13
 */
@Slf4j
//@Component
//@RocketMQMessageListener(topic = JmsConfig.TOPIC, selectorExpression = "testtag1", consumerGroup = "test_consumer", messageModel = MessageModel.CLUSTERING /* 消息类型：集群、广播*/)
public class FamilyMQListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt message) {
        try {
            String body = new String(message.getBody(), "utf-8");
            log.info("Consumer-获取消息-主题topic为={}, 消费消息为={}", message.getTopic(), body);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}

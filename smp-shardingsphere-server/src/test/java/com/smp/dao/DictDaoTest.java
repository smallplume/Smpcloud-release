package com.smp.dao;

import com.smp.SmpShardingsphereServerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author SmallPlume
 * @title: DictDaoTest
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/6/21 17:23
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SmpShardingsphereServerApplication.class})
public class DictDaoTest {

    @Autowired
    private DictDao dictDao;

    @Test
    public void insertDict() {
        dictDao.insertDict("user_type","5","管理员");
        dictDao.insertDict("user_type","6","操作员");
    }

    @Test
    public void deleteDict() {
        dictDao.deleteDict(3L);
    }

}

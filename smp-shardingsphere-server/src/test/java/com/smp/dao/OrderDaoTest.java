package com.smp.dao;

import com.smp.SmpShardingsphereServerApplication;
import com.smp.entry.Order;
import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author:fangxiaoyu1900@126.com
 * @date:2020/4/13 , 10:08
 * @version:1.0
 * @description:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SmpShardingsphereServerApplication.class})
public class OrderDaoTest {

    @Autowired
    private OrderDao orderDao;

    @Test
    public void testInsertOrder() {
        for (int i = 0; i < 10; i++) {
            orderDao.insertOrder(new BigDecimal((i + 1) * 5), 1L, "WAIT_PAY");
        }
        for (int i = 0; i < 10; i++) {
            orderDao.insertOrder(new BigDecimal((i + 1) * 5), 2L, "WAIT_PAY");
        }
    }

    @Test
    public void testSelectOrderbyIds() {
        List<Long> ids = new ArrayList<>();
        ids.add(456602077179674625L);
        ids.add(456602076680552449L);
        List<Order> maps = orderDao.selectOrderbyIds(ids);
        System.out.println(">>>>>>>>>>>" + maps);
    }

    @Test
    public void testSelectOrderByUserId() {
        List<Order> maps = orderDao.selectOrderByUserId(1);
        System.out.println("Size ===== " + maps.size());
        System.out.println(maps);
    }

    @Test
    public void testSelectOrderbyUserAndIds() {
        List<Long> orderIds = new ArrayList<>();
        orderIds.add(456623637479292928L);
        orderIds.add(456623637173108737L);
        //查询条件中包括分库的键user_id
        int user_id = 1;
        List<Order> orders = orderDao.selectOrderByUserAndIds(user_id, orderIds);
        JSONArray jsonOrders = new JSONArray(orders);
        System.out.println(jsonOrders);
    }
}

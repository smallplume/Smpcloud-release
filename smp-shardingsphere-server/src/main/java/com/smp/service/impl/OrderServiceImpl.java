package com.smp.service.impl;

import com.smp.dao.OrderDao;
import com.smp.entry.Order;
import com.smp.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author:fangxiaoyu1900@126.com
 * @date:2020/4/13 , 13:01
 * @version:1.0
 * @description:
 **/
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertA(Order order) {
        return orderDao.insertOrder(new BigDecimal(order.getPrice()), order.getUserId(), order.getStatus());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertB() {
        orderDao.insertOrder(new BigDecimal(2), 2L, "PAPA");
        int i = 10/0;
        return i;
    }

    @Override
    public List<Order> queryByOrderIds(List<Long> orderIds) {
        return orderDao.selectOrderbyIds(orderIds);
    }
}

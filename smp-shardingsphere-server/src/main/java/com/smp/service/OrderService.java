package com.smp.service;

import com.smp.entry.Order;

import java.util.List;

/**
 * @author:fangxiaoyu1900@126.com
 * @date:2020/4/13 , 12:37
 * @version:1.0
 * @description:
 **/
public interface OrderService {

    int insertA(Order order);

    int insertB();

    List<Order> queryByOrderIds(List<Long> orderIds);
}

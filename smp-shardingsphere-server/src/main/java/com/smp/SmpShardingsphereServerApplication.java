package com.smp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmpShardingsphereServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmpShardingsphereServerApplication.class, args);
    }

}

package com.smp.entry;

import lombok.Data;

/**
 * @author SmallPlume
 * @title: Dict
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/6/21 17:16
 */
@Data
public class Dict {

    private Long dictId;
    /**
     * 字典类型
     */
    private String type;
    /**
     * 字典编码
     */
    private String code;
    /**
     * 字典值
     */
    private String value;

}

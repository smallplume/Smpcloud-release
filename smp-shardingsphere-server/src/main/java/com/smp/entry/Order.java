package com.smp.entry;

import lombok.Data;

/**
 * @author:fangxiaoyu1900@126.com
 * @date:2020/4/13 , 12:55
 * @version:1.0
 * @description:
 **/
@Data
public class Order {

    private Long orderId;

    private Integer price;

    private Long userId;

    private String status;

}

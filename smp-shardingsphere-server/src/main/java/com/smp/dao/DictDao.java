package com.smp.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * @author SmallPlume
 * @title: DictDao
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/6/21 17:18
 */
@Mapper
@Component
public interface DictDao {

    @Insert("insert into t_dict(type, code, value) values(#{type}, #{code}, #{value})")
    int insertDict(@Param("type") String type, @Param("code") String code, @Param("value") String value);

    @Delete("delete from t_dict where dict_id = #{dictId}")
    int deleteDict(@Param("dictId") Long dictId);
}

package com.smp.dao;

import com.smp.entry.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author:359428217@qq.com
 * @date:2020/4/13 , 10:00
 * @version:1.0
 * @description:
 **/
@Mapper
@Component
public interface OrderDao {

    /**
     * 新增订单
     *
     * @param price  订单价格
     * @param userId 用户id
     * @param status 订单状态
     * @return
     */
    @Insert("insert into t_order(price,user_id,status) value(#{price},#{userId},#{status})")
    int insertOrder(@Param("price") BigDecimal price, @Param("userId") Long userId,
                    @Param("status") String status);

    /**
     * 根据id列表查询多个订单
     *
     * @param orderIds 订单id列表
     * @return
     */
    @Select({"<script>" +
            "select " +
            " * " +
            " from t_order t" +
            " where t.order_id in " +
            "<foreach collection='orderIds' item='id' open='(' separator=',' close=')'>" +
            " #{id} " +
            "</foreach>" +
            "</script>"})
    List<Order> selectOrderbyIds(@Param("orderIds") List<Long> orderIds);

    /**
     * 根据用户ID查询
     *
     * @param userId
     * @return
     */
    @Select("select * from t_order t where t.user_id = #{userId}")
    List<Order> selectOrderByUserId(@Param("userId") Integer userId);

    /**
     *
     * @param userId
     * @param orderIds
     * @return
     */
    @Select({"<script>",
            " select",
            " * ",
            " from t_order t",
            "where t.order_id in",
            "<foreach collection='orderIds' item='id' open='(' separator=',' close=')'>",
            "#{id}",
            "</foreach>",
            " and t.user_id = #{userId} ",
            "</script>"})
    List<Order> selectOrderByUserAndIds(@Param("userId") Integer userId, @Param("orderIds") List<Long> orderIds);

}

package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.entry.Order;
import com.smp.service.OrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author:fangxiaoyu1900@126.com
 * @date:2020/4/13 , 13:02
 * @version:1.0
 * @description:
 **/
@RestController
@RequestMapping("/sharding")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/insert")
    public BaseVo insertOrder(@RequestBody Order order) {
        for (int i = 1; i <= 20; i++) {
            Order entry = new Order();
            int r = (int) (Math.random() * 100000) % 100;
            entry.setUserId(Long.decode(String.valueOf(r)));
            entry.setStatus(order.getStatus());
            entry.setPrice(order.getPrice());
            orderService.insertA(entry);
        }
        return new BaseVo();
    }

    @GetMapping("/query")
    public BaseVo queryByOrderIds(@RequestParam(value = "ids", required = false) String ids) {
        long[] idsArray = Arrays.stream(StringUtils.split(ids, ',')).mapToLong(Long::valueOf).toArray();
        List<Long> list = new ArrayList<>(idsArray.length);
        for (int i = 0; i < idsArray.length; i++) {
            list.add(idsArray[i]);
        }
        List<Order> orderList = orderService.queryByOrderIds(list);
        return new BaseVo(orderList);
    }

    @GetMapping("/testTransaction")
    public BaseVo textTransaction() {
        orderService.insertB();
        return new BaseVo();
    }

}

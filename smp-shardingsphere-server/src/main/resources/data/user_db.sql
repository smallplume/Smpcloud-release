/*
 Navicat Premium Data Transfer

 Source Server         : Centos7-01的MySQL7
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 192.168.137.128:3306
 Source Schema         : user_db

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 25/06/2020 22:31:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典id',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类型',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典值',
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_dict
-- ----------------------------
INSERT INTO `t_dict` VALUES (2, 'user_type', '1', '操作员');
INSERT INTO `t_dict` VALUES (4, 'user_type', '5', '管理员');
INSERT INTO `t_dict` VALUES (5, 'user_type', '6', '操作员');
INSERT INTO `t_dict` VALUES (6, 'user_type', '5', '管理员');
INSERT INTO `t_dict` VALUES (7, 'user_type', '5', '管理员');
INSERT INTO `t_dict` VALUES (8, 'user_type', '6', '操作员');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户姓名',
  `user_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户类型',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, '姓名1', NULL);
INSERT INTO `t_user` VALUES (2, '姓名2', NULL);
INSERT INTO `t_user` VALUES (3, '姓名3', NULL);
INSERT INTO `t_user` VALUES (4, '姓名4', NULL);
INSERT INTO `t_user` VALUES (5, '姓名5', NULL);
INSERT INTO `t_user` VALUES (6, '姓名6', NULL);
INSERT INTO `t_user` VALUES (7, '姓名7', NULL);
INSERT INTO `t_user` VALUES (8, '姓名8', NULL);
INSERT INTO `t_user` VALUES (9, '姓名9', NULL);
INSERT INTO `t_user` VALUES (10, '姓名10', NULL);

SET FOREIGN_KEY_CHECKS = 1;

/*
 Navicat Premium Data Transfer

 Source Server         : Centos7-01的MySQL7
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 192.168.137.128:3306
 Source Schema         : order_db1

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 25/06/2020 22:28:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典id',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类型',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典值',
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_dict
-- ----------------------------
INSERT INTO `t_dict` VALUES (2, 'user_type', '1', '操作员');
INSERT INTO `t_dict` VALUES (4, 'user_type', '5', '管理员');
INSERT INTO `t_dict` VALUES (5, 'user_type', '6', '操作员');
INSERT INTO `t_dict` VALUES (6, 'user_type', '5', '管理员');
INSERT INTO `t_dict` VALUES (7, 'user_type', '5', '管理员');
INSERT INTO `t_dict` VALUES (8, 'user_type', '6', '操作员');

-- ----------------------------
-- Table structure for t_order_1
-- ----------------------------
DROP TABLE IF EXISTS `t_order_1`;
CREATE TABLE `t_order_1`  (
  `order_id` bigint(20) NOT NULL COMMENT '主键ID',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_order_1
-- ----------------------------
INSERT INTO `t_order_1` VALUES (481463794023792640, 120.00, 84, '有效的');
INSERT INTO `t_order_1` VALUES (481463794111873024, 120.00, 70, '有效的');
INSERT INTO `t_order_1` VALUES (481463794149621760, 120.00, 20, '有效的');
INSERT INTO `t_order_1` VALUES (481463794191564800, 120.00, 8, '有效的');
INSERT INTO `t_order_1` VALUES (481463794258673664, 120.00, 64, '有效的');
INSERT INTO `t_order_1` VALUES (481463794350948352, 120.00, 64, '有效的');

-- ----------------------------
-- Table structure for t_order_2
-- ----------------------------
DROP TABLE IF EXISTS `t_order_2`;
CREATE TABLE `t_order_2`  (
  `order_id` bigint(20) NOT NULL COMMENT '主键ID',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_order_2
-- ----------------------------
INSERT INTO `t_order_2` VALUES (481463793654693889, 120.00, 2, '有效的');
INSERT INTO `t_order_2` VALUES (481463793965072385, 120.00, 0, '有效的');
INSERT INTO `t_order_2` VALUES (481463794044764161, 120.00, 30, '有效的');
INSERT INTO `t_order_2` VALUES (481463794132844545, 120.00, 48, '有效的');
INSERT INTO `t_order_2` VALUES (481463794216730625, 120.00, 40, '有效的');
INSERT INTO `t_order_2` VALUES (481463794292228097, 120.00, 84, '有效的');
INSERT INTO `t_order_2` VALUES (481463794329976833, 120.00, 68, '有效的');

SET FOREIGN_KEY_CHECKS = 1;

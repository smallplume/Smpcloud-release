/*
 Navicat Premium Data Transfer

 Source Server         : Centos7-01的MySQL7
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : 192.168.137.128:3306
 Source Schema         : order_db2

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 25/06/2020 22:30:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典id',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类型',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典值',
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_dict
-- ----------------------------
INSERT INTO `t_dict` VALUES (2, 'user_type', '1', '操作员');
INSERT INTO `t_dict` VALUES (4, 'user_type', '6', '操作员');
INSERT INTO `t_dict` VALUES (5, 'user_type', '5', '管理员');
INSERT INTO `t_dict` VALUES (6, 'user_type', '5', '管理员');
INSERT INTO `t_dict` VALUES (7, 'user_type', '6', '操作员');

-- ----------------------------
-- Table structure for t_order_1
-- ----------------------------
DROP TABLE IF EXISTS `t_order_1`;
CREATE TABLE `t_order_1`  (
  `order_id` bigint(20) NOT NULL COMMENT '主键ID',
  `price` decimal(10, 2) NOT NULL COMMENT '价格',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_order_1
-- ----------------------------
INSERT INTO `t_order_1` VALUES (481463793939906560, 120.00, 51, '有效的');
INSERT INTO `t_order_1` VALUES (481463793981849600, 120.00, 25, '有效的');
INSERT INTO `t_order_1` VALUES (481463794061541376, 120.00, 49, '有效的');
INSERT INTO `t_order_1` VALUES (481463794313199616, 120.00, 97, '有效的');

-- ----------------------------
-- Table structure for t_order_2
-- ----------------------------
DROP TABLE IF EXISTS `t_order_2`;
CREATE TABLE `t_order_2`  (
  `order_id` bigint(20) NOT NULL COMMENT '主键ID',
  `price` decimal(10, 2) NOT NULL COMMENT '价格',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_order_2
-- ----------------------------
INSERT INTO `t_order_2` VALUES (481463794002821121, 120.00, 79, '有效的');
INSERT INTO `t_order_2` VALUES (481463794082512897, 120.00, 33, '有效的');
INSERT INTO `t_order_2` VALUES (481463794170593281, 120.00, 81, '有效的');

SET FOREIGN_KEY_CHECKS = 1;

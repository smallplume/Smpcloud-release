package com.smp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmpGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmpGatewayApplication.class, args);
    }

}

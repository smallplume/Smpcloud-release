package com.smp.controller;

import com.smp.api.UserFacade;
import com.smp.common.BaseVo;
import com.smp.dto.UserDTO;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

/**
 * @author:359428217@qq.com
 * @date:2020/3/29 , 22:21
 * @version:1.0
 * @description:
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Reference(version = "1.0.0")
    private UserFacade userFacade;

    @PostMapping("/insert")
    public BaseVo insert(@RequestBody UserDTO userDTO) {
        UserDTO userDTO1 = userFacade.inc(userDTO);
        return new BaseVo(userDTO1);
    }

    @GetMapping("/transaction")
    public BaseVo testTransaction() {
        userFacade.insertA();
        return new BaseVo();
    }

}

package com.smp;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@NacosPropertySource(dataId = "smp-dubbo", autoRefreshed = true)
public class SmpDubboConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmpDubboConsumerApplication.class, args);
    }

}

package com.smp.controller;

import com.smp.common.BaseVo;
import com.smp.dto.CompanyDTO;
import com.smp.dto.UserDTO;
import com.smp.service.TransactionFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author SmallPlume
 * @title: TransactionController
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/7 0:36
 */
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionFacade transactionFacade;

    @GetMapping("/add")
    public BaseVo insert() {
        UserDTO userDTO = new UserDTO();
        userDTO.setName("老王");
        userDTO.setBalance(12345L);
        userDTO.setCompanyName("阿里巴巴");
        CompanyDTO companyDTO = new CompanyDTO();
        transactionFacade.add(userDTO, companyDTO);
        return new BaseVo();
    }

    @GetMapping("/getNacosValue")
    public BaseVo getNacosTest() {
        return new BaseVo(transactionFacade.getNacosTest());
    }

    @GetMapping("/user/query")
    public BaseVo findUserById(@RequestParam(value = "id") String id) {
        return new BaseVo(transactionFacade.findUserById(id));
    }
}

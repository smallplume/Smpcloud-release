package com.smp.service.impl;

import com.smp.common.BaseVo;
import com.smp.dto.CompanyDTO;
import com.smp.dto.UserDTO;
import com.smp.service.CompanyServiceFeign;
import com.smp.service.TransactionFacade;
import com.smp.service.UserServiceFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author SmallPlume
 * @title: TransactionFacadeImpl
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/7 0:33
 */
@Slf4j
@Service
public class TransactionFacadeImpl implements TransactionFacade {

    @Autowired
    private UserServiceFeign userServiceFeign;

    @Autowired
    private CompanyServiceFeign companyServiceFeign;

    @Override
    public void add(UserDTO userDTO, CompanyDTO companyDTO) {
        UserDTO user = userServiceFeign.insert(userDTO);
        companyDTO.setLegalPerson(user.getId());
        companyDTO.setName(user.getCompanyName());
        companyServiceFeign.insert(companyDTO);
    }

    @Override
    public String getNacosTest() {
        return userServiceFeign.getNocasTest();
    }

    @Override
    public UserDTO findUserById(String id) {
        BaseVo<UserDTO> user = userServiceFeign.findUserById(id);
        return user.getData();
    }

}

package com.smp.service;

import com.smp.dto.CompanyDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author SmallPlume
 * @title: CompanyServiceFeign
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/7 0:30
 */
@FeignClient(value = "nacos-server-02")
public interface CompanyServiceFeign {

    @RequestMapping(value = "/company/insert", method = RequestMethod.POST)
    void insert(@RequestBody CompanyDTO companyDTO);
}

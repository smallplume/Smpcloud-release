package com.smp.service;

import com.smp.common.BaseVo;
import com.smp.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author SmallPlume
 * @title: UserServiceFeign
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/7 0:27
 */
@FeignClient(value = "nacos-server-01")
public interface UserServiceFeign {

    @RequestMapping(value = "/user/insert", method = RequestMethod.POST)
    UserDTO insert(@RequestBody UserDTO userDTO);

    @RequestMapping(value = "/user/getNacosTest", method = RequestMethod.GET)
    String getNocasTest();

    @RequestMapping(value = "/user/query", method = RequestMethod.GET)
    BaseVo<UserDTO> findUserById(@RequestParam(value = "id") String id);

}

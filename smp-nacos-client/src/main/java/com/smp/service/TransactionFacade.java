package com.smp.service;

import com.smp.dto.CompanyDTO;
import com.smp.dto.UserDTO;

/**
 * @author SmallPlume
 * @title: TransactionServiceFeign
 * @projectName Smpcloud-release
 * @description: TODO
 * @date 2020/3/7 0:25
 */
public interface TransactionFacade {

    void add(UserDTO userDTO, CompanyDTO companyDTO);

    String getNacosTest();

    UserDTO findUserById(String id);

}

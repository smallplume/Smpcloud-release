package com.smp.config;

import com.netflix.loadbalancer.*;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Feign的日志配置 NONE,BASIC,HEADERS,FULL
 * 生产环境只要配置 BASIC就行了，性能问题
 *
 * @autor xiaoyu.fang
 * @date 2019/12/12 11:19
 */
@Configuration
public class FeignConfig {

    @Bean
    Logger.Level feignLoggerLevel() {
        /**
         * NONE 不输出日志
         * BASIC 只有请求方法、URL、响应状态代码、执行时间
         * HEADERS基本信息以及请求和响应头
         * FULL 请求和响应 的heads、body、metadata，建议使用这个级别
         */
        return Logger.Level.FULL;
    }

    /**
     * 重新创建一个均衡策略，表示不使用默认
     *
     * @return
     */
    @Bean
    public IRule getIReule() {
        // 通过获取一个IRule对象，
        // 达到的目的，用我们重新选择的随机，替代默认的轮训方式
        // return new RoundRobinRule(); // 轮询
        // return new RandomRule(); // 随机
        // return new AvailabilityFilteringRule(); // 过滤掉故障，剩下的进行轮询访问
        return new WeightedResponseTimeRule(); // 根据权重
        // return new RetryRule(); // 先按照RoundRobinRule，如果失败进行重试
        // return new BestAvailableRule(); // 过滤故障，选择并发量最小的服务
        // return new ZoneAvoidanceRule(); // 默认规则，复合判断server所在区域的性能和server的可用选择服务器
    }
}
